#!/bin/bash

MIGRATION_TABLE='migrations'
MIGRATION_DIR=
MIGRATE_ALL=0

DB_FILE=data.db

MODE=
COMMAND=

function run_sqlite {
  CMD="$*"
  echo -e "\033[1;34mrunning\033[0m $CMD..."
  sqlite3 $DB_FILE "$CMD"
}

function parse_options {
  ID=0
  while [ $# -ge 1 ]; do
    case $1 in
      -c|--create) MODE=create;;
      -A|--all) MIGRATE_ALL=1;;
      -U|--up) MIGRATION_DIR=up;;
      -D|--down) MIGRATION_DIR=down;;
      -m|--migrate) MODE=migrate;;
      --destroy) MODE=destroy;;
      -x|--exec) 
        MODE=exec;
        shift;
        if [ -z "$1" ]; then
          die "exec require parameter (SQL query)";
        fi
        COMMAND="$1"
        ;;
      -l|--list) MODE=list;;
      -s|--show) MODE=show;;
      -h|--help) MODE=help;;
      *) die "unknown option: $1";;
    esac
    ID=$(( $ID + 1 ))
    shift
  done
  [ ! -z "$MODE" ] || usage
}

function usage {
  echo -e "\033[1;34musage\033[0m: db.sh [OPTIONS...]"
  echo
  echo -e "* One of the following options must be given:"
  echo
  echo -e "\t-c, --create: create the database"
  echo -e "\t    --destroy: destroy the database"
  echo -e "\t-l, --list: list the whole database"
  echo -e "\t-x, --exec: execute a command in the database"
  echo -e "\t-s, --show: create the database"
  echo
  echo -e "\t-m, --migrate: migrate the database"
  echo -e "\t-U, --up: apply next migration file (use with --migrate)"
  echo -e "\t-D, --down: revert last migration file (use with --migrate)"
  echo -e "\t-A, --all: apply sequentially all migration scripts (use with --up/--down)"
  exit 42
}

function die {
  echo -e "\033[1;31merror\033[0m: $*" >/dev/stderr
  exit 84
}

function mode_create() {
  if [ ! -e "$DB_FILE" ]; then
    run_sqlite "$(cat data/up.sql)" || die "failed to create db" && echo "db created"
  fi
}

function mode_destroy() {
  if [ -e "$DB_FILE" ]; then
    rm -i "$DB_FILE"
  fi
}

function mode_exec() {
    run_sqlite "$COMMAND"
}

function mode_list() {
  run_sqlite "select * from aggregate;"
}

function mode_migrate_up {
  NUM_MIGS=$(sqlite3 "$DB_FILE" 'select count(*) from migrations order by id desc')
  LAST_MIG=$(sqlite3 $DB_FILE 'select name from migrations order by id desc limit 1' | head)
  LAST_MIG_ID=$(echo $LAST_MIG | cut -d '-' -f 1)
  LAST_MIG_NAME=$(echo $LAST_MIG | cut -d '-' -f 2)
  NEXT_MIG_ID=$(( $LAST_MIG_ID + 1 ))
  NEXT_MIG_FILE=$(find data -mindepth 2 -iname 'up.sql' | grep -E "^data/$(($NEXT_MIG_ID))-")
  if [ -z "$NEXT_MIG_FILE" ]; then
    die "no more migrations"
  fi
  NEXT_MIG_NAME=$(echo $NEXT_MIG_FILE | grep -Po '^data\/\K[\d]+-[\-\w\s]+')
  if [ "$NUM_MIGS" = "0" ]; then
    echo -e "\033[1;34mrunning\033[0m first migration \033[0;32m$NEXT_MIG_NAME\033[0m..."
  else
    echo -e "\033[1;34mapplying\033[0m migration #${NEXT_MIG_ID} '\033[0;32m${NEXT_MIG_NAME}\033[0m' (was at #$LAST_MIG_ID '$LAST_MIG_NAME')"
  fi
  sqlite3 "$DB_FILE" <"$NEXT_MIG_FILE" && sqlite3 "$DB_FILE" "insert into migrations (id, name) values (NULL, '$NEXT_MIG_NAME')"
}

function mode_migrate_dn {
  NUM_MIGS=$(sqlite3 "$DB_FILE" 'select count(*) from migrations order by id desc')
  if [ "$NUM_MIGS" = "0" ]; then
    die "no more migrations to rollback"
  fi
  LAST_MIG=$(sqlite3 "$DB_FILE" 'select name from migrations order by id desc limit 1' | head)
  LAST_MIG_ID=$(echo $LAST_MIG | cut -d '-' -f 1)
  LAST_MIG_NAME=$(echo $LAST_MIG | cut -d '-' -f 2)
  MIG_SCRIPT=$(find data -mindepth 2 -iname 'down.sql' | grep -E "^data/$LAST_MIG_ID-")
  echo -ne "\033[1;34mrolling back\033[0m \033[0;32m$LAST_MIG\033[0m... "
  sqlite3 "$DB_FILE" <"$MIG_SCRIPT"
  STATUS_CODE=$?
  if [ ! "$STATUS_CODE" = "0" ]; then
    die "failed to roll back migration"
  else
    sqlite3 "$DB_FILE" "delete from migrations where name = '$LAST_MIG'" || die "failed to write migration update to db"
  fi
  CUR_MIG=$(sqlite3 $DB_FILE 'select name from migrations order by id desc limit 1' | head)
  NUM_MIGS=$(sqlite3 "$DB_FILE" 'select count(*) from migrations order by id desc')
  if [ "$NUM_MIGS" = "0" ]; then
    echo -e "now @\033[0;33mroot\033[0m"
  else
    echo -e "now @\033[0;33m$CUR_MIG\033[0m"
  fi
  return $STATUS_CODE
}

function mode_migrate() {
  [ ! -z "$MIGRATION_DIR" ] || die "no action given, use --up/--down"
  case "$MIGRATION_DIR" in
    up)
      if [ "$MIGRATE_ALL" = "1" ]; then
        DONE=0
        while [ $DONE = 0 ]; do
          mode_migrate_up || DONE=1
        done
      else
        mode_migrate_up || die "failed to migrate"
      fi
      ;;
    down)
      if [ "$MIGRATE_ALL" = "1" ]; then
        DONE=0
        while [ $DONE = 0 ]; do
          mode_migrate_dn || DONE=1
        done
      else
        mode_migrate_dn || die "failed to migrate"
      fi
      ;;
    *) die "unknown migration dir: $MIGRATION_DIR"
  esac
}

function mode_show() {
  echo "not implemented"
  exit 168
}

parse_options $*
case $MODE in
  create) mode_create;;
  destroy) mode_destroy;;
  exec) mode_exec;;
  migrate) mode_migrate;;
  list) mode_list;;
  show) mode_show;;
  help) usage;;
  *) die "unknown mode '$1'"
esac