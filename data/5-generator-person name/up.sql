INSERT INTO `dataset` (`id`, `domain`, `name`, `type`,`attribs`) VALUES (NULL, 1, 'person name', 'data', '');

create temporary table vars (`name` text, `value` text);
insert into vars values ('id', (SELECT id FROM `dataset` where `name` = 'person name'));

INSERT INTO `data` (`id`, `dataset`, `locale`, `name`, `value`)
VALUES
  (NULL, (select `value` from `vars` where `name` = 'id'), 2, 'first name', 'georges'),
  (NULL, (select `value` from `vars` where `name` = 'id'), 2, 'first name', 'william'),
  (NULL, (select `value` from `vars` where `name` = 'id'), 2, 'first name', 'harry'),
  (NULL, (select `value` from `vars` where `name` = 'id'), 2, 'first name', 'peter'),
  (NULL, (select `value` from `vars` where `name` = 'id'), 1, 'first name', 'pierre'),
  (NULL, (select `value` from `vars` where `name` = 'id'), 1, 'first name', 'henri'),
  (NULL, (select `value` from `vars` where `name` = 'id'), 1, 'first name', 'jacques'),
  (NULL, (select `value` from `vars` where `name` = 'id'), 1, 'first name', 'charles-henry'),
  (NULL, (select `value` from `vars` where `name` = 'id'), 1, 'last name', 'de la villardiere'),
  (NULL, (select `value` from `vars` where `name` = 'id'), 1, 'last name', 'le gros');
