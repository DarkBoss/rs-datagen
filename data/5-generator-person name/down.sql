drop table if exists vars;
create temporary table vars (`name` text, `value` text);
insert into vars values ('id', (SELECT id FROM `dataset` WHERE `name` = 'person name'));

delete from `data` where `dataset` = (SELECT `value` FROM `vars` WHERE `name` = 'id');
delete from `dataset` where id = (SELECT `value` FROM `vars` WHERE `name` = 'id');