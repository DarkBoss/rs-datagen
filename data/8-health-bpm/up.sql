drop table if exists vars;
create temporary table vars (`name` text, `value` text);

insert into vars values ('domain_id', (SELECT id FROM `domain` where `name` = 'health'));

INSERT INTO `dataset` (`id`, `domain`, `name`, `type`,`attribs`) VALUES (NULL, (select `value` from `vars` where `name` = 'domain_id' ), 'health bpm', 'sequence', 'rand(40..190)');

insert into vars values ('dataset_id', (SELECT id FROM `dataset` where `name` = 'health bpm'));