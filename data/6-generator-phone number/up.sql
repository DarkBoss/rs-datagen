INSERT INTO `dataset` (`id`, `domain`, `name`, `type`,`attribs`) VALUES (NULL, 1, 'phone number', 'data', '');

drop table if exists vars;
create temporary table vars (`name` text, `value` text);
insert into vars values ('id', (SELECT id FROM `dataset` where `name` = 'phone number'));
insert into vars values ('locale_fr', (SELECT id FROM `locale` where `name` = 'fra'));
insert into vars values ('locale_en', (SELECT id FROM `locale` where `name` = 'eng'));

INSERT INTO `data` (`id`, `dataset`, `locale`, `name`, `value`)
VALUES
  (NULL, (select `value` from vars where `name` = 'id'), (select `value` from vars where `name` = 'locale_fr'), 'phone number', '+336 XX XX XX'),
  (NULL, (select `value` from vars where `name` = 'id'), (select `value` from vars where `name` = 'locale_fr'), 'phone number', '+337 XX XX XX'),
  (NULL, (select `value` from vars where `name` = 'id'), (select `value` from vars where `name` = 'locale_en'), 'phone number', '+555 XXX XXX');