drop table if exists vars;
create temporary table vars (`name` text, `value` text);

insert into vars values ('domain_id', (SELECT id FROM `domain` where `name` = 'health'));
insert into vars values ('locale_fr', (SELECT id FROM `domain` where `name` = 'fra'));
insert into vars values ('locale_en', (SELECT id FROM `domain` where `name` = 'eng'));

INSERT INTO `dataset` (`id`, `domain`, `name`, `type`,`attribs`) VALUES (NULL, (select `value` from `vars` where `name` = 'domain_id' ), 'health ssn', 'data', '');

insert into vars values ('dataset_id', (SELECT id FROM `dataset` where `name` = 'health ssn'));

INSERT INTO `data` (`id`, `dataset`, `locale`, `name`, `value`)
VALUES
	(NULL, (select `value` from vars where `name` = 'dataset_id'), (select `value` from vars where `name` = 'locale_fr'), 'SSN', 'XXX XX XXX XX');