INSERT INTO `dataset` (`id`, `domain`, `name`, `type`,`attribs`)
VALUES
  (NULL, 1, 'person age', 'sequence', 'rand(1..100)'),
  (NULL, 1, 'person birthday', 'data', ''),
  (NULL, 2, 'country name', 'data', ''),
  (NULL, 2, 'country identifier', 'data', ''),
  (NULL, 4, 'password generation sentence', 'data', '');

