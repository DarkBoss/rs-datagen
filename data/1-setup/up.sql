-- Drop tables in reverse order
DROP VIEW IF EXISTS `aggregate`;
DROP TABLE IF EXISTS `data`;
DROP TABLE IF EXISTS `dataset`;
DROP TABLE IF EXISTS `domain`;
DROP TABLE IF EXISTS `locale`;

-- Create tables
CREATE TABLE `domain` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`name`	INTEGER NOT NULL UNIQUE
);

CREATE TABLE `locale` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`name`	INTEGER NOT NULL UNIQUE
);

CREATE TABLE `dataset` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`domain`	INTEGER NOT NULL,
	`name`	TEXT NOT NULL UNIQUE,
	`type`	TEXT NOT NULL,
	`attribs`	TEXT NOT NULL,
	FOREIGN KEY(`domain`) REFERENCES `domain`(`id`) ON DELETE SET NULL
);

CREATE TABLE `data` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`dataset`	INTEGER NOT NULL,
  `locale` INTEGER NULL,
	`name`	TEXT NOT NULL,
	`value`	TEXT NOT NULL,
	FOREIGN KEY(`dataset`) REFERENCES `dataset`(`id`) ON DELETE SET NULL,
	FOREIGN KEY(`locale`) REFERENCES `locale`(`id`) ON DELETE SET NULL
);

CREATE VIEW aggregate AS
    SELECT locale.name AS locale_name,
           domain.name AS domain_name,
           dataset.name AS dataset_name,
           data.name AS data_name,
           data.value AS data_value,
           domain.id AS domain_id,
           data.id AS data_id,
           dataset.id AS dataset_id,
           locale.id AS locale_id
      FROM data,
           dataset,
           domain,
           locale
     WHERE data.locale = locale.id AND 
           data.dataset = dataset.id AND 
           dataset.domain = domain.id
    UNION
    SELECT 'NULL' AS locale_name,
           domain.name AS domain_name,
           dataset.name AS dataset_name,
           data.name AS data_name,
           data.value AS data_value,
           domain.id AS domain_id,
           data.id AS data_id,
           dataset.id AS dataset_id,
           'NULL' AS locale_id
      FROM data,
           dataset,
           domain,
           locale
     WHERE data.locale IS NULL AND 
           data.dataset = dataset.id AND 
           dataset.domain = domain.id
     GROUP BY domain_name,
              dataset_name,
              data_name
     ORDER BY domain_name,
              dataset_name,
              data_name DESC;