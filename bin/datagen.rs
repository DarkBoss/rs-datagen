extern crate clap;
extern crate console;
extern crate datagen;
extern crate rusqlite;
extern crate log;

use log::error;

use datagen::{App, Config, Error, Generator, RandomPasswordGenerator, data::*, Result};
use console::style;

fn try_main() -> Result<()> {
  App::new()
    .setup()?
    .run()?;
  Ok(())
}

fn main() {
  if let Err(e) = try_main() {
    match e {
      Error::SilentExit => {},
      _ => /*error!*/eprintln!("{}: {}", "error", e.to_string())
    }
  }
}
