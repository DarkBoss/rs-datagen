use crate::result::app_result::AppResultList;
use crate::result::app_result::AppResult;
use crate::config::Config;
use crate::data::{BoxedSource, Data};
use crate::error::Error;
use crate::generator::Generator;
use crate::result::Result;
use crate::random_replace::RandomReplace;

use crate::config::{GENERATOR_TIMEOUT_SECONDS, DEFAULT_CHARSET};

use rand::{rngs::ThreadRng, Rng};

use crate::config::DEFAULT_LIMIT;

use std::collections::HashMap;

#[derive(Clone)]
pub struct PhoneNumberGenerator {
  name: String,
  config: Config,
  data: Vec<Data>,
  rng: rand::rngs::ThreadRng,
}

impl std::default::Default for PhoneNumberGenerator {
  fn default() -> PhoneNumberGenerator {
    PhoneNumberGenerator {
      name: String::from("phone number"),
      config: Config::default(),
      rng: rand::thread_rng(),
      data: vec![]
    }
  }
}

impl Generator for PhoneNumberGenerator {
  fn configure(&mut self, conf: &Config) -> Result<()>
  where
    Self: std::marker::Sized,
  {
    self.config = conf.clone();
    Ok(())
  }

  fn config(&self) -> &Config {
    &self.config
  }

  fn name(&self) -> &String {
    &self.name
  }

  fn load_source(&mut self, s: &BoxedSource) -> Result<()> {
    let dataset = s.datasets().iter().find(|ds| ds.name == "phone number").unwrap();
    self.data = s.datas().iter().filter(|d| d.dataset == dataset.id).map(|d| d.clone()).collect();
    Ok(())
  }

  fn generate_one(&mut self) -> Result<AppResult> {
    let mut res = String::new();
    if self.data.len() == 0 {
      return Err(Error::Custom("no datas in data_source!".to_string()));
    }
    let id = self.rng.gen_range(0, self.data.len());
    let mut template: String = self.data.get(id).unwrap().value.clone();
    Ok(AppResult::Simple(RandomReplace::new("X".to_string()).replace_by_digits(template.as_str())))
  }
}

impl PhoneNumberGenerator {
  pub fn with_config(conf: &Config) -> Result<PhoneNumberGenerator> {
    let mut g = PhoneNumberGenerator::default();
    g.configure(conf)?;
    Ok(g)
  }
}

#[cfg(test)]
mod tests {
  use crate::data::mock::MockSource;
  use crate::setup_logger;
  use crate::setup_testing;
  use super::*;

  #[test]
  fn person_name_generator_should_output_something() {
    use regex::Regex;
    use crate::data::Source;
    use crate::data::model::*;
    const PERSON_NAMES: [&str;3] = [ "georges", "harris", "frank" ];
    setup_testing(Some(Box::new(|source: &mut BoxedSource| {
      let domain_id = 1;
      let dataset_id = 1;
      let locale_id = 1;
      
      let mut domains = source.domains_mut();
      domains.push(Domain::new(domain_id, "person".to_string()));
      
      let mut datasets = source.datasets_mut();
      datasets.push(Dataset::new(dataset_id, domain_id, "person name".to_string(), "data".to_string(), "".to_string()));
      
      let mut locales = source.locales_mut();
      locales.push(Locale::new(locale_id, "eng".to_string()));
      
      let mut data = source.datas_mut();
      PERSON_NAMES.iter().enumerate()
        .for_each(|(id, name)| {
          data.push(Data::new(id as i64, dataset_id, locale_id, "first name".to_string(), name.to_string()))
        });
      Ok(())
    }))).expect("failed to setup test");
    let mut g = PhoneNumberGenerator::default();
    match g.generate_one() {
      Ok(ref res) => match res {
        AppResult::Simple(name) => assert_eq!(PERSON_NAMES.contains(&name.as_str()), true),
        AppResult::Complex(map) => panic!("invalid complex result for generate_one"),
        AppResult::None => panic!("invalid empty result for generate_one")
      },
      Err(e) => panic!(e.to_string())
    };
  }
}