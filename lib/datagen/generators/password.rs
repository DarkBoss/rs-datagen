use crate::result::app_result::AppResult;
use crate::result::app_result::AppResultList;
use crate::config::Config;
use crate::generator::Generator;
use crate::result::Result;

use rand::{Rng, rngs::ThreadRng};

use crate::config::DEFAULT_LIMIT;

pub const RANDOM_PASSWORD_NAME: &'static str = "random password";

use crate::data::BoxedSource;

#[derive(Clone)]
pub struct RandomPasswordGenerator {
  name: String,
  config: Config,
  rng: rand::rngs::ThreadRng
}

impl std::default::Default for RandomPasswordGenerator {
  fn default() -> RandomPasswordGenerator {
    RandomPasswordGenerator {
      name: String::from(RANDOM_PASSWORD_NAME),
      config: Config::default(),
      rng: rand::thread_rng(),
    }
  }
}

impl Generator for RandomPasswordGenerator {
  fn configure(&mut self, conf: &Config) -> Result<()> where Self: std::marker::Sized {
    self.config = conf.clone();
    Ok(())
  }

  fn config(&self) -> &Config {
    &self.config
  }

  fn load_source(&mut self, s: &BoxedSource) -> Result<()> {
    Ok(())
  }

  fn name(&self) -> &String {
    &self.name
  }

  fn generate_one(&mut self) -> Result<AppResult> {
    let mut res = String::new();
    let chosen_len = self.rng.gen_range(self.config.min_length, self.config.max_length);
    for _i in 0..chosen_len {
      let ch = self.generate_char()? as char;
      res.push(ch);
    }
    Ok(AppResult::Simple(res))
  }
}

impl RandomPasswordGenerator {
  pub fn with_config(conf: &Config) -> Result<RandomPasswordGenerator> {
    let mut g = RandomPasswordGenerator::default();
    g.configure(conf)?;
    Ok(g)
  }

  fn generate_char(&mut self) -> Result<char> {
    let charset = self.config.charset.as_bytes();
    let id: usize = self.rng.gen_range(0, charset.len());
    let ch: u8 = charset[id];
    Ok(ch as char)
  }

}

#[cfg(test)]
mod tests {
  use crate::setup_logger;
  use crate::setup_testing;
  use super::*;

  #[test]
  fn random_password_generator_should_output_something() {
    use regex::Regex;
    let cfg = setup_testing(None).expect("failed to setup test");
    let mut g = RandomPasswordGenerator::default();
    match g.generate_one() {
      Ok(res) => {
        match res {
          AppResult::Simple(name) => assert_ne!(name, ""),
          AppResult::Complex(map) => panic!("invalid complex result for generate_one"),
          AppResult::None => panic!("invalid empty result for generate_one")
        }
      },
      Err(e) => panic!(e.to_string())
    };
  }
}