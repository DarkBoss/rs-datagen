use crate::config::Config;
use crate::data::BoxedSource;
use crate::data::Data;
use crate::error::Error;
use crate::generator::Generator;
use crate::result::app_result::AppResult;
use crate::result::app_result::AppResultList;
use crate::result::Result;
use crate::timer::Timer;
use super::{PersonNameGenerator, HealthSSNGenerator, PhoneNumberGenerator};
use rand::{rngs::ThreadRng, Rng};

use crate::config::DEFAULT_LIMIT;

use std::collections::HashMap;

use crate::config::GENERATOR_TIMEOUT_SECONDS;

#[derive(Clone)]
pub struct PersonGenerator {
  name: String,
  config: Config,
  name_generator: PersonNameGenerator,
  ssn_generator: HealthSSNGenerator,
  phone_number_generator: PhoneNumberGenerator
}

impl std::default::Default for PersonGenerator {
  fn default() -> PersonGenerator {
    PersonGenerator {
      name: String::from("person"),
      config: Config::default(),
      name_generator: PersonNameGenerator::default(),
      ssn_generator: HealthSSNGenerator::default(),
      phone_number_generator: PhoneNumberGenerator::default()
    }
  }
}

#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct Person {
  pub name: String,
  pub phone_number: String,
  pub ssn: String,
}

impl std::fmt::Display for Person {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{} {} {}", self.name, self.phone_number, self.ssn)
  }
}

impl std::convert::Into<HashMap<String, String>> for Person {
  fn  into(self) -> HashMap<String, String> {
    use std::iter::FromIterator;
    HashMap::<String, String>::from_iter(vec![
      ("name".to_string(), self.name),
      ("phone_number".to_string(), self.phone_number),
      ("ssn".to_string(), self.ssn),
    ].into_iter())
  }
}

impl Generator for PersonGenerator {
  fn configure(&mut self, conf: &Config) -> Result<()>
  {
    self.config = conf.clone();
    self.name_generator.configure(&self.config)?;
    self.phone_number_generator.configure(&self.config)?;
    self.ssn_generator.configure(&self.config)?;
    Ok(())
  }

  fn config(&self) -> &Config {
    &self.config
  }
  fn load_source(&mut self, s: &BoxedSource) -> Result<()> {
    self.name_generator.load_source(s)?;
    self.phone_number_generator.load_source(s)?;
    self.ssn_generator.load_source(s)?;
    Ok(())
  }

  fn name(&self) -> &String {
    &self.name
  }

  fn generate_one(&mut self) -> Result<AppResult> {
    let mut p = Person::default();
    p.name = format!("{}", self.name_generator.generate_one()?);
    p.phone_number = format!("{}", self.phone_number_generator.generate_one()?);
    p.ssn = format!("{}", self.ssn_generator.generate_one()?);
    Ok(AppResult::Complex(p.into()))
  }
}

impl PersonGenerator {
  pub fn with_config(conf: &Config) -> Result<PersonGenerator> {
    let mut g = PersonGenerator::default();
    g.configure(conf)?;
    Ok(g)
  }
}