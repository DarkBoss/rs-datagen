use crate::config::Config;
use crate::data::BoxedSource;
use crate::data::Data;
use crate::error::Error;
use crate::generator::Generator;
use crate::result::app_result::AppResult;
use crate::result::app_result::AppResultList;
use crate::result::Result;
use crate::timer::Timer;

use rand::{rngs::ThreadRng, Rng};

use crate::config::DEFAULT_LIMIT;

use std::collections::HashMap;

use crate::config::GENERATOR_TIMEOUT_SECONDS;

#[derive(Clone)]
pub struct CountryCodeGenerator {
  name: String,
  config: Config,
  rng: rand::rngs::ThreadRng,
  data: Vec<Data>,
}

impl std::default::Default for CountryCodeGenerator {
  fn default() -> CountryCodeGenerator {
    CountryCodeGenerator {
      name: String::from("country code"),
      config: Config::default(),
      rng: rand::thread_rng(),
      data: vec![],
    }
  }
}

impl Generator for CountryCodeGenerator {
  fn configure(&mut self, conf: &Config) -> Result<()>
  where
    Self: std::marker::Sized,
  {
    self.config = conf.clone();
    Ok(())
  }

  fn config(&self) -> &Config {
    &self.config
  }
  fn load_source(&mut self, s: &BoxedSource) -> Result<()> {
    let dataset = s
      .datasets()
      .iter()
      .find(|ds| ds.name == "country list")
      .unwrap();
    self.data = s
      .datas()
      .iter()
      .filter(|d| d.dataset == dataset.id)
      .map(|d| d.clone())
      .collect();
    Ok(())
  }

  fn name(&self) -> &String {
    &self.name
  }

  fn generate_one(&mut self) -> Result<AppResult> {
    let mut res = String::new();
    if self.data.len() == 0 {
      return Err(Error::Custom("no datas in data_source!".to_string()));
    }
    let id = self.rng.gen_range(0, self.data.len());
    Ok(AppResult::Simple(self.data.get(id).unwrap().value.clone()))
  }
}

impl CountryCodeGenerator {
  pub fn with_config(conf: &Config) -> Result<CountryCodeGenerator> {
    let mut g = CountryCodeGenerator::default();
    g.configure(conf)?;
    Ok(g)
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::data::mock::MockSource;
  use crate::setup_logger;
  use crate::setup_testing;

  #[test]
  fn country_code_generator_should_output_something() {
    use crate::data::model::*;
    use crate::data::Source;
    use regex::Regex;
    const COUNTRY_CODES: [&str; 3] = ["Ireland", "England", "France"];
    setup_testing(Some(Box::new(|source: &mut BoxedSource| {
      let domain_id = 1;
      let dataset_id = 1;
      let locale_id = 1;
      let mut domains = source.domains_mut();
      domains.push(Domain::new(domain_id, "person".to_string()));
      let mut datasets = source.datasets_mut();
      datasets.push(Dataset::new(
        dataset_id,
        domain_id,
        "country name".to_string(),
        "data".to_string(),
        "".to_string(),
      ));
      let mut locales = source.locales_mut();
      locales.push(Locale::new(locale_id, "eng".to_string()));
      let mut data = source.datas_mut();
      COUNTRY_CODES.iter().enumerate().for_each(|(id, name)| {
        data.push(Data::new(
          id as i64,
          dataset_id,
          locale_id,
          "first name".to_string(),
          name.to_string(),
        ))
      });
      Ok(())
    })))
    .expect("failed to setup test");
    let mut g = CountryCodeGenerator::default();
    match g.generate_one() {
      Ok(ref res) => match res {
        AppResult::Simple(name) => assert_eq!(COUNTRY_CODES.contains(&name.as_str()), true),
        AppResult::Complex(map) => panic!("invalid complex result for generate_one"),
        AppResult::None => panic!("invalid empty result for generate_one"),
      },
      Err(e) => panic!(e.to_string()),
    };
  }
}
