use crate::result::app_result::AppResultList;
use crate::result::app_result::AppResult;
use crate::config::Config;
use crate::data::{BoxedSource, Data};
use crate::error::Error;
use crate::generator::Generator;
use crate::result::Result;
use crate::random_replace::RandomReplace;

use crate::config::{GENERATOR_TIMEOUT_SECONDS, DEFAULT_CHARSET};

use rand::{rngs::ThreadRng, Rng};

use crate::config::DEFAULT_LIMIT;

use std::collections::HashMap;

#[derive(Clone)]
pub struct HealthSSNGenerator {
  name: String,
  config: Config,
  data: Vec<Data>,
  rng: rand::rngs::ThreadRng,
}

impl std::default::Default for HealthSSNGenerator {
  fn default() -> HealthSSNGenerator {
    HealthSSNGenerator {
      name: String::from("health ssn"),
      config: Config::default(),
      rng: rand::thread_rng(),
      data: vec![]
    }
  }
}

impl Generator for HealthSSNGenerator {
  fn configure(&mut self, conf: &Config) -> Result<()>
  where
    Self: std::marker::Sized,
  {
    self.config = conf.clone();
    Ok(())
  }

  fn config(&self) -> &Config {
    &self.config
  }

  fn name(&self) -> &String {
    &self.name
  }

  fn load_source(&mut self, s: &BoxedSource) -> Result<()> {
    let dataset = s.datasets().iter().find(|ds| ds.name == "health ssn").unwrap();
    self.data = s.datas().iter().filter(|d| d.dataset == dataset.id).map(|d| d.clone()).collect();
    Ok(())
  }

  fn generate_one(&mut self) -> Result<AppResult> {
    let mut res = String::new();
    if self.data.len() == 0 {
      return Err(Error::Custom("no datas in data_source!".to_string()));
    }
    let id = self.rng.gen_range(0, self.data.len());
    let mut template: String = self.data.get(id).unwrap().value.clone();
    Ok(AppResult::Simple(RandomReplace::new("X".to_string()).replace_by_digits(template.as_str())))
  }
}

impl HealthSSNGenerator {
  pub fn with_config(conf: &Config) -> Result<HealthSSNGenerator> {
    let mut g = HealthSSNGenerator::default();
    g.configure(conf)?;
    Ok(g)
  }
}