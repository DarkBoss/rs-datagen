use crate::result::app_result::AppResultList;
use crate::result::app_result::AppResult;
use crate::config::Config;
use crate::data::{Data, BoxedSource};
use crate::error::Error;
use crate::generator::Generator;
use crate::result::Result;

const CAPTURE_SEQUENCES: &str =
  r"(rand\(|\{)\s*([\d]+)\s*\.\.\s*([\d]+)\s*(?:\+=){0,1}\s*([\d\.]+){0,1}\s*(\)|\})";

#[derive(Debug, Clone)]
pub struct SequenceBuilder<T> {
  rng: rand::rngs::ThreadRng,
  min: T,
  max: T,
  step: T,
  random: bool,
  current: T,
}

pub trait One<T> {
  fn one() -> T;
}

impl One<i32> for i32 {
  fn one() -> i32 {
    1
  }
}
impl One<i64> for i64 {
  fn one() -> i64 {
    1
  }
}
impl One<f32> for f32 {
  fn one() -> f32 {
    1.0
  }
}
impl One<f64> for f64 {
  fn one() -> f64 {
    1.0
  }
}

use rand::{rngs::ThreadRng, Rng};

impl<T> SequenceBuilder<T>
where
  T: std::marker::Sized,
  T: std::ops::Add<T, Output = T> + Copy + Clone,
  T: std::cmp::PartialOrd,
  T: std::fmt::Debug,
  T: std::fmt::Display,
  T: std::str::FromStr,
  T: std::default::Default,
  <T as std::str::FromStr>::Err: std::string::ToString,
  <T as std::ops::Add>::Output: std::cmp::PartialOrd,
  T: One<T>,
  T: rand::distributions::uniform::SampleUniform,
{
  pub fn new_random(min: T, max: T) -> SequenceBuilder<T> {
    SequenceBuilder {
      rng: rand::thread_rng(),
      min,
      max,
      step: T::one(),
      random: true,
      current: min,
    }
  }

  pub fn new_increment(min: T, max: T, step: T) -> SequenceBuilder<T> {
    SequenceBuilder {
      rng: rand::thread_rng(),
      min,
      max,
      step,
      random: false,
      current: min,
    }
  }

  pub fn get(&self) -> T {
    self.current
  }

  pub fn is_random(&self) -> bool {
    self.random
  }

  pub fn random(&self) -> &bool {
    &self.random
  }

  pub fn random_mut(&mut self) -> &mut bool {
    &mut self.random
  }

  pub fn max(&self) -> &T {
    &self.max
  }
  pub fn min(&self) -> &T {
    &self.min
  }
  pub fn step(&self) -> &T {
    &self.step
  }
  pub fn current(&self) -> &T {
    &self.current
  }

  pub fn max_mut(&mut self) -> &mut T {
    &mut self.max
  }
  pub fn min_mut(&mut self) -> &mut T {
    &mut self.min
  }
  pub fn step_mut(&mut self) -> &mut T {
    &mut self.step
  }
  pub fn current_mut(&mut self) -> &mut T {
    &mut self.current
  }

  pub fn take(&mut self) -> Option<T> {
    use std::cmp::PartialOrd;
    use std::ops::Add;
    let mut next: T = self.current + self.step;
    if self.random {
      next = self.rng.gen_range(self.min, self.max);
    }
    if next >= self.max {
      return None;
    }
    let ret: T = match self.random {
      true => next,
      false => self.current,
    };
    self.current = next;
    Some(ret)
  }

  pub fn parse<S>(s: S) -> Result<Option<SequenceBuilder<T>>>
  where
    S: AsRef<str>,
    S: std::fmt::Display,
  {
    let rule =
      regex::Regex::new(CAPTURE_SEQUENCES).or_else(|e| Err(Error::Custom(e.to_string())))?;
    let mut icap = 0;
    let mut builder: Option<SequenceBuilder<T>> = None;
    let mut min: T;
    let mut max: T;
    for cap in rule.captures_iter(s.as_ref()) {
      let mut step: T = T::one();
      if let Some(c) = cap.get(4) {
        step = c
          .as_str()
          .parse::<T>()
          .or_else(|e| Err(Error::Custom(e.to_string())))?;
      }
      if cap[1].to_string().contains("rand") {
        builder = Some(SequenceBuilder::new_random(
          cap[2].parse::<T>().or_else(|e| {
            Err(Error::Custom(format!(
              "cannot parse min '{}', {}",
              s,
              e.to_string()
            )))
          })?,
          cap[3].parse::<T>().or_else(|e| {
            Err(Error::Custom(format!(
              "cannot parse max '{}', {}",
              s,
              e.to_string()
            )))
          })?,
        ));
      } else {
        builder = Some(SequenceBuilder::new_increment(
          cap[2].parse::<T>().or_else(|e| {
            Err(Error::Custom(format!(
              "cannot parse min '{}', {}",
              s,
              e.to_string()
            )))
          })?,
          cap[3].parse::<T>().or_else(|e| {
            Err(Error::Custom(format!(
              "cannot parse max '{}', {}",
              s,
              e.to_string()
            )))
          })?,
          step,
        ));
      }
      if icap > 0 {
        return Err(Error::Custom(format!(
          "multiple sequence expressions: '{}'",
          s
        )));
      }
      icap += 1;
    }
    if icap == 0 {
      return Ok(None);
    }
    Ok(builder)
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn sequence_builders_should_correctly_increment() {
    let mut seq = SequenceBuilder::<i64>::new_increment(0, 100, 1);
    assert_eq!(seq.take().is_some(), true);
    assert_eq!(seq.take().unwrap(), 1); // already incremented by last line
    assert_eq!(seq.take().unwrap(), 2);
  }

  #[test]
  fn sequence_builders_should_parse_simple_sequence() {
    let seq_str = "{1..10}";
    let seq = SequenceBuilder::<i64>::parse(seq_str);
    if seq.is_err() {
      panic!("{}", seq.err().unwrap().to_string());
    }
    assert_eq!(seq.is_ok(), true);
    let seq1 = seq.unwrap();
    assert_eq!(seq1.is_some(), true);
    let sq = &seq1.unwrap();
    assert_eq!(*sq.min(), 1);
    assert_eq!(*sq.max(), 10);
    assert_eq!(*sq.step(), 1);
    assert_eq!(*sq.current(), 1);
  }

  #[test]
  fn sequence_builders_should_parse_step_sequence() {
    let seq_str = "{1..10+=1}";
    let seq = SequenceBuilder::<i64>::parse(seq_str);
    if seq.is_err() {
      panic!("{}", seq.err().unwrap().to_string());
    }
    assert_eq!(seq.is_ok(), true);
    let seq1 = seq.unwrap();
    assert_eq!(seq1.is_some(), true);
    let sq = &seq1.unwrap();
    assert_eq!(*sq.min(), 1);
    assert_eq!(*sq.max(), 10);
    assert_eq!(*sq.step(), 1);
    assert_eq!(*sq.current(), 1);
  }

  #[test]
  fn sequence_builders_should_parse_random_sequence() {
    let seq_str = "rand(1..10)";
    let mut seq = SequenceBuilder::<i64>::parse(seq_str);
    if seq.is_err() {
      panic!("{}", seq.err().unwrap().to_string());
    }
    assert_eq!(seq.is_ok(), true);
    let mut seq1 = seq.unwrap();
    assert_eq!(seq1.is_some(), true);
    let mut sq = &mut seq1.unwrap();
    assert_eq!(*sq.min(), 1);
    assert_eq!(*sq.max(), 10);
    assert_eq!(*sq.step(), 1);
    assert_eq!(*sq.current(), 1);

    let mut taken: Vec<i64> = vec![];
    for i in 0..4 {
      taken.push(sq.take().expect("invalid random item"));
    }
    assert_ne!(taken, vec![1, 2, 3, 4]);
  }
}

pub struct SequenceGenerator {
  name: String,
  config: Config,
  builder: SequenceBuilder<i64>
}

impl std::default::Default for SequenceGenerator {
  fn default() -> Self {
    Self {
      name: "Sequence".to_string(),
      config: Config::default(),
      builder: SequenceBuilder::new_increment(0, 1000, 1),
    }
  }
}

impl Generator for SequenceGenerator {
  fn configure(&mut self, conf: &Config) -> Result<()>
  where
    Self: std::marker::Sized,
  {
    self.config = conf.clone();
    Ok(())
  }

  fn config(&self) -> &Config {
    &self.config
  }

  fn load_source(&mut self, s: &BoxedSource) -> Result<()> {
    Ok(())
  }

  fn name(&self) -> &String {
    &self.name
  }

  fn generate_one(&mut self) -> Result<AppResult> {
    Ok(AppResult::Simple(format!("{}", self.builder.take().unwrap_or(0 as i64))))
  }
}

impl SequenceGenerator {
  pub fn with_config(name: Option<String>, conf: &Config, seq_def: String) -> Result<SequenceGenerator> {
    let mut g = SequenceGenerator::default();
    g.configure(conf)?;
    if name.is_some() {
      g.name = name.unwrap();
    }
    g.set_definition(&seq_def);
    Ok(g)
  }

  pub fn set_definition(&mut self, def: &String) -> Result<()> {
    let b = SequenceBuilder::parse(def)?;
    self.builder = b.ok_or(Error::Custom(format!("invalid sequence '{}'", def)))?;
    Ok(())
  }
}
