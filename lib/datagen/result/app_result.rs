use std::collections::HashMap;

// Represent a final result, either simple (values) 
// or complex (objects)
#[derive(Clone, PartialEq, Eq)]
pub enum AppResult {
  None,
  Simple(String),
  Complex(HashMap<String, String>),
}

// A list of final results
pub type AppResultList = Vec<AppResult>;

impl AppResult {
  // Retrieve the string kind of this result
  pub fn kind(&self) -> String {
    match self {
      AppResult::Simple(_) => "AppResult::Simple",
      AppResult::Complex(_) => "AppResult::Complex",
      AppResult::None => "AppResult::None",
    }
    .to_string()
  }
}

impl std::fmt::Display for AppResult {
  // Print results
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(
      f,
      "{}",
      match self {
        AppResult::Simple(v) => v.to_string(),
        AppResult::Complex(m) => format!("{} ({} items)", self.kind(), m.len()),
        AppResult::None => format!("None"),
      }
    )
  }
}

impl std::fmt::Debug for AppResult {
  // Debug results
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(
      f,
      "{}",
      match self {
        AppResult::Simple(v) => format!("{} ({})", self.kind(), v),
        AppResult::Complex(m) =>
          m.iter()
            .enumerate()
            .fold(String::new(), |accu, (id, (k, v))| format!(
              "{}{}{}[{}][{}] = '{}'",
              accu,
              match id {
                _ if id > 0 => "\n",
                _ => "",
              },
              self.kind(),
              id,
              k,
              v
            )),
        AppResult::None => format!("None"),
      }
    )
  }
}
