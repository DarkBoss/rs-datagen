use crate::result::csv_writer::CSV_WRITER_VSEP;
use crate::result::csv_writer::CSV_WRITER_QUOTE;
use crate::result::csv_writer::CSV_WRITER_HSEP;
use crate::result::app_result::AppResultList;
use std::io::{Write, Result as IOResult};
use crate::result::app_result::AppResult;
use crate::result::Result;

#[derive(Clone)]
pub struct WriterConfig {
  pub sep_h: String,
  pub sep_v: String,
  pub quote_ch: String,
}

impl std::default::Default for WriterConfig {
  fn default() -> WriterConfig {
    WriterConfig::new(CSV_WRITER_HSEP, CSV_WRITER_VSEP, CSV_WRITER_QUOTE)
  }
}

impl WriterConfig {
  fn new<S: AsRef<str>>(sep_h: S, sep_v: S, quote_ch: S) -> WriterConfig {
    WriterConfig {
      sep_h: sep_h.as_ref().to_string(),
      sep_v: sep_v.as_ref().to_string(),
      quote_ch: quote_ch.as_ref().to_string(),
    }
  }
}

pub trait Writer {
  fn name(&self) -> String;
  fn ext(&self) -> String;
  
  fn configure(&mut self, cfg: &WriterConfig) -> Result<()>;

  fn write_value(&self, v: &String, qch: char) -> String {
    let is_alpha = !v.chars()
      .filter(|ch| !ch.is_numeric())
      .collect::<Vec<char>>()
      .is_empty();
    if is_alpha {
      format!("{quote}{value}{quote}", quote = qch, value = self.escape_quotes(v))
    } else {
      v.to_string()
    }
  }

  fn escape_quotes(&self, s: &String) -> String {
    use regex::Regex;
    let find_starting = Regex::new("^\"").unwrap();
    let find_non_escaped = Regex::new("(?P<r>[^\\\\])\"").unwrap();
    let ret = find_starting.replace_all(s, "\\\"");
    find_non_escaped.replace_all(&ret.to_string(), "$r\\\"").to_string()
  }

  fn write<W>(&mut self, w: &mut W, res: &AppResultList) -> IOResult<()>
    where
      W: std::io::Write,
      Self: Sized;
}
pub type BoxedWriter<'a> = Box<dyn Writer + 'a>;