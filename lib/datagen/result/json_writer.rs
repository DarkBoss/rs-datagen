use crate::result::app_result::AppResult;
use crate::result::app_result::AppResultList;
use crate::result::writer::Writer;
use crate::result::writer::WriterConfig;
use crate::result::Result;
use std::collections::HashMap;
use std::io::Write;

pub const JSON_WRITER_NAME: &'static str = "Javascript Object Notation";
pub const JSON_WRITER_EXT: &'static str = "json";
pub const JSON_WRITER_COMMA: &'static str = ",";
pub const JSON_WRITER_ARRAY: (&'static str, &'static str) = ("[", "]");
pub const JSON_WRITER_OBJECT: (&'static str, &'static str) = ("{", "}");
pub const JSON_WRITER_QUOTE: &'static str = "\"";

#[derive(Default, Clone)]
pub struct JSONWriter {
  config: WriterConfig,
}

impl JSONWriter {
  pub fn new() -> JSONWriter {
    JSONWriter {
      config: WriterConfig::default(),
    }
  }
}

impl Writer for JSONWriter {
  fn name(&self) -> String {
    return JSON_WRITER_NAME.to_string();
  }

  fn ext(&self) -> String {
    return JSON_WRITER_EXT.to_string();
  }

  fn configure(&mut self, cfg: &WriterConfig) -> Result<()> {
    self.config = cfg.clone();
    Ok(())
  }

  fn write<W: Write>(&mut self, w: &mut W, res: &AppResultList) -> std::io::Result<()> {
    use std::iter::FromIterator;
    let mut ret: Vec<String> = res.iter().enumerate().map(|(id, row)| match row {
      AppResult::Simple(ref v) => self.write_value(v, '"'),
      AppResult::Complex(map) => {
        format!("{}{}{}", JSON_WRITER_OBJECT.0, map.iter().fold(String::new(), |mut accu, (k, v)| {
          if !accu.is_empty() {
            accu = format!("{}{} ", accu, JSON_WRITER_COMMA);
          }
          format!("{}{quote}{key}{quote}: {value}", accu, key = self.escape_quotes(k), value = self.write_value(v, '"'), quote = JSON_WRITER_QUOTE)
        }), JSON_WRITER_OBJECT.1)
      },
      AppResult::None => {
        "NULL".to_string()
      }
    }).collect();
    let s = ret
      .iter()
      .fold(String::new(), |mut accu, item| {
        if !accu.is_empty() {
          accu = format!("{}{} ", accu, JSON_WRITER_COMMA);
        }
        format!("{}{}", accu, item)
      });
    write!(w, "{}{}{}", JSON_WRITER_ARRAY.0, s, JSON_WRITER_ARRAY.1)?;
    Ok(())
  }
}
