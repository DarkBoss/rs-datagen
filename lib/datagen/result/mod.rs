pub mod writer;
pub mod csv_writer;
pub mod json_writer;
pub mod app_result;

pub use writer::*;
pub use csv_writer::*;
pub use json_writer::*;
pub use app_result::*;

use super::error::Error;

pub type Result<T> = std::result::Result<T, Error>;