use crate::result::writer::WriterConfig;
use crate::result::app_result::AppResult;
use crate::result::app_result::AppResultList;
use crate::result::writer::Writer;
use crate::result::Result;
use std::collections::HashMap;
use std::io::Write;

pub const CSV_WRITER_NAME: &'static str = "Comma Separated Values";
pub const CSV_WRITER_EXT: &'static str = "csv";
pub const CSV_WRITER_HSEP: &'static str = ",";
pub const CSV_WRITER_VSEP: &'static str = "\n";
pub const CSV_WRITER_QUOTE: &'static str = "\"";

// A writer capable of writing csv-formatted ouput
#[derive(Default, Clone)]
pub struct CSVWriter {
  header: Vec<String>,
  rows: Vec<Vec<String>>,
  config: WriterConfig
}

impl CSVWriter {
  // Instanciate the writer
  pub fn new() -> CSVWriter {
    CSVWriter {
      header: vec![],
      rows: vec![],
      config: WriterConfig::default(),
    }
  }

  // Build csv cells from result list
  pub fn build_results<W: Write>(&mut self, w: &mut W, res: &AppResultList) {
    if !res.is_empty() {
      match res.first().unwrap() {
        AppResult::Complex(first_res) => {
          if self.header.is_empty() {
            self.header = first_res
              .keys()
              .map(|k| k.to_string())
              .collect::<Vec<String>>()
              .clone();
          }
          res.iter().for_each(|r| {
            if let AppResult::Complex(cr) = r {
              // add back missing header values
              let row_keys: Vec<&String> = cr.keys().collect();
              row_keys.iter().for_each(|k| {
                if !self.header.contains(k) {
                  self.header.push(k.to_string());
                }
              });
              // add row, by iterating over headers and adding
              // corresponding row values
              self.rows.push(
                self
                  .header
                  .iter()
                  .fold(Vec::<String>::new(), |mut accu, header| {
                    accu.push(cr.get(header).expect("missing csv header").clone());
                    accu
                  }),
              );
            }
          });
        }
        AppResult::Simple(v) => {
          self.header.clear();
          self.header.push("id".to_string());
          self.header.push("value".to_string());
          res
            .iter()
            .enumerate()
            .for_each(|(id, r)| self.rows.push(vec![format!("{}", id), r.to_string()]));
        }
        AppResult::None => {}
      }
    }
  }

  // Write the results previously built
  fn do_write<W: Write>(&self, w: &mut Write) -> std::io::Result<()> {
    let h_str = self
      .header
      .iter()
      .enumerate()
      .fold(String::new(), |accu, (id, header)| {
        format!(
          "{}{}{quote}{}{quote}",
          accu,
          match id {
            _ if id > 0 => CSV_WRITER_HSEP,
            _ => "",
          },
          self.escape_quotes(header),
          quote = CSV_WRITER_QUOTE
        )
      });
    let mut r_str = String::new();
    let mut row_id: usize = 0;
    for r in &self.rows {
      r_str = format!(
        "{}{}",
        r_str,
        r.iter()
          .enumerate()
          .fold(String::new(), |accu, (cell_id, row)| {
            format!(
              "{}{}",
              match row_id {
                _ if row_id > 0 => CSV_WRITER_VSEP,
                _ => "",
              },
              self.header.iter().enumerate().fold(
                String::new(),
                |accu, (header_id, header)| format!(
                  "{}{}{quote}{}{quote}",
                  accu,
                  match header_id {
                    // horz separator if not first header
                    _ if header_id > 0 => CSV_WRITER_HSEP.to_string(),
                    // nothing otherwise
                    _ => String::new(),
                  },
                  self.escape_quotes(r.get(header_id as usize).expect("missing row value")),
                  quote = CSV_WRITER_QUOTE
                )
              ),
            )
          })
      );
      row_id += 1;
    }
    write!(w, "{}{}{}", h_str, CSV_WRITER_VSEP, r_str)
  }

  // Retrieve the header row
  pub fn header(&self) -> &Vec<String> {
    &self.header
  }

  // Retrieve the header row as mutable
  pub fn header_mut(&mut self) -> &mut Vec<String> {
    &mut self.header
  }
}

// The Writer implementation of the CSVWriter
impl Writer for CSVWriter {
  // The name of this writer
  fn name(&self) -> String {
    return CSV_WRITER_NAME.to_string();
  }

  // The extension this writer handles
  fn ext(&self) -> String {
    return CSV_WRITER_EXT.to_string();
  }

  // Change the configuration
  fn configure(&mut self, cfg: &WriterConfig) -> Result<()> {
    self.config = cfg.clone();
    Ok(())
  }

  // Write build results to output stream
  fn write<W: Write>(&mut self, w: &mut W, res: &AppResultList) -> std::io::Result<()> {
    self.build_results(w, res);
    self.do_write::<W>(w)
  }
}


#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn csv_writer_should_escape_quotes_correctly() {
    let mut w = CSVWriter::default();
    let actual = w.escape_quotes(&"\"test\"".to_string());
    assert_eq!(actual, "\\\"test\\\"".to_string());
  }
}