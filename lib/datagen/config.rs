use crate::result::csv_writer::CSV_WRITER_NAME;
use crate::data::SourceConfig;
use crate::data::SupportedType;
use std::time::Duration;

#[derive(Clone, Debug)]
pub enum Verbosity {
  None,
  Low,
  Medium,
  High
}

impl std::convert::Into<log::LevelFilter> for Verbosity {
  fn into(self) -> log::LevelFilter {
    match self {
      Verbosity::None => log::LevelFilter::Off,
      Verbosity::Low => log::LevelFilter::Warn,
      Verbosity::Medium => log::LevelFilter::Info,
      Verbosity::High => log::LevelFilter::max()
    }
  }
}
impl std::default::Default for Verbosity {
  fn default() -> Verbosity {
    Verbosity::Medium
  }
}

impl Verbosity {
  pub fn parse<S: AsRef<str>>(s: S) -> Option<Verbosity> {
    use std::iter::FromIterator;
    let mut map: std::collections::HashMap<String, Verbosity> = std::collections::HashMap::new();
    map.insert("none".to_string(), Verbosity::None);
    map.insert("low".to_string(), Verbosity::Low);
    map.insert("medium".to_string(), Verbosity::Medium);
    map.insert("high".to_string(), Verbosity::High);
    let vs = s.as_ref().to_string().to_ascii_lowercase();
    if map.contains_key(&vs) {
      Some(map.get(&vs).unwrap().clone())
    } else {
      None
    }
  }
}

#[derive(Clone, Debug)]
pub struct Config {
  pub data_sources: Vec<SourceConfig>,  // data sources configuration
  pub min_length: i64,                  // minimum word length
  pub max_length: i64,                  // maximum word length
  pub charset: String,                  // character set to be used
  pub interactive: bool,                // interactively ask values
  pub generator: String,                // which generator to use
  pub limit: i64,                       // max number of words to generate
  pub output_format: String,            // name of output format
  pub locale: String,                   // user locale to use
  pub dump_data: bool,                  // true if should dump data and exit
  pub verbosity: Verbosity,             // verbose level
  pub timeout: Option<Duration>         // maximum time spent generating
}

pub const DEFAULT_LOCALE: &'static str = "eng";

pub const DEFAULT_MIN_LENGTH: i64 = 3;
pub const DEFAULT_MAX_LENGTH: i64 = 10;
pub const DEFAULT_LIMIT: i64 = 10;
pub const DEFAULT_VERBOSITY: Verbosity = Verbosity::Medium;
pub const DEFAULT_CHARSET: &'static str = "abcdefghijklmnopqrstuvwxyz0123456789,./;'[]\\|}{\":?><";
pub const DEFAULT_OUTPUT_FORMAT: &'static str = CSV_WRITER_NAME;
pub const MAIN_DATA_SOURCE: &'static str = "data.db";

pub const GENERATOR_TIMEOUT_SECONDS: f32 = 5.0;

impl std::default::Default for Config {
  fn default() -> Config {
    Config {
      data_sources: vec![
        SourceConfig::with_values(MAIN_DATA_SOURCE.to_string(), true, String::new(), SupportedType::Sqlite),
      ],
      generator: String::from("random password"),
      min_length: DEFAULT_MIN_LENGTH,
      max_length: DEFAULT_MAX_LENGTH,
      dump_data: false,
      output_format: DEFAULT_OUTPUT_FORMAT.to_string(),
      locale: String::from(DEFAULT_LOCALE),
      interactive: false,
      charset: String::from(DEFAULT_CHARSET),
      limit: DEFAULT_LIMIT,
      verbosity: Verbosity::default(),
      timeout: Some(std::time::Duration::from_secs_f32(GENERATOR_TIMEOUT_SECONDS))
    }
  }
}