use crate::config::{Verbosity, DEFAULT_LOCALE};
use crate::data::*;
use crate::error::Error;
use crate::result::app_result::AppResultList;
use crate::result::writer::BoxedWriter;
use crate::result::AppResult;
use crate::result::Result;
use crate::setup_logger;
use crate::standard_writer_fns;
use crate::standard_writers;
use crate::config::GENERATOR_TIMEOUT_SECONDS;
use crate::BoxedWriterFn;
use crate::SequenceGenerator;
use crate::Timer;
use crate::WriterFnMap;

use crate::{
  data::BoxedSourceList, standard_generators, BoxedGenerator, BoxedGeneratorList, Config, Generator,
};
use crate::{get_locale, set_locale};
use log::*;

use clap::App as CliApp;
use clap::{Arg, SubCommand};
use console::{style, Term};

use std::io::Write;

const DEFAULT_LOGGER_CONFIG: &'static str = "log4rs.yml";

pub struct App {
  config: Config,
  generators: BoxedGeneratorList,
  writers: Vec<BoxedWriter<'static>>,
  writer_fns: WriterFnMap,
  terminal: Term,
}

impl App {
  pub fn new() -> App {
    App {
      config: Config::default(),
      generators: standard_generators(),
      writers: standard_writers(),
      writer_fns: standard_writer_fns(),
      terminal: Term::stdout(),
    }
  }

  pub fn config(&self) -> &Config {
    &self.config
  }

  pub fn config_mut(&mut self) -> &mut Config {
    &mut self.config
  }

  pub fn setup(&mut self) -> Result<&mut App> {
    self.parse_options()?;

    setup_logger(self.config.verbosity.clone().into())?;
    info!("logging initialized");

    self.setup_locale()?;
    self.setup_sources()?;

    if self.config.dump_data {
      for source in sources() {
        dump_source(source, &mut std::io::stdout())?;
      }
      return Err(Error::SilentExit);
    }
    if self.config.interactive {
      self.configure()?;
    }
    Ok(self)
  }

  fn setup_sources(&mut self) -> Result<()> {
    crate::data::setup_sources(&self.config)?;
    for dataset in self.sequence_datasets() {
      self.generators.push(Box::new(SequenceGenerator::with_config(Some(dataset.name.clone()), &self.config, dataset.attribs.clone())?));
    }
    Ok(())
  }

  fn generate(&mut self, gn: String) -> Result<(AppResultList, &BoxedGenerator)> {
    let mut found_gen = self.generators.iter_mut().find(|g| *g.name() == gn);
    let gen: &mut Box<dyn Generator>;
    if found_gen.is_none() {
      return Err(Error::UnknownGenerator(self.config.generator.clone()));
    }
    gen = found_gen.unwrap();
    info!("Generating {} {}...", self.config.limit, gen.name());
    gen.configure(&self.config)?;
    for source in sources() {
      gen.load_source(source)?;
    }
    Ok((gen.generate()?, gen))
  }

  pub fn run(&mut self) -> Result<&mut Self> {
    let timer = Timer::new();
    let (words, gen) = self.generate(self.config.generator.clone())?;
    let work_duration = timer.total_elapsed().as_secs_f32();
    info!("Done in {}s", work_duration);
    self.write_results(&words, &mut std::io::stdout())?;
    let write_duration = timer.total_elapsed().as_secs_f32();
    info!("Written in {}s", write_duration);
    info!("Total time: {}s", work_duration + write_duration);
    Ok(self)
  }

  fn write_results<W: std::io::Write>(&mut self, res: &AppResultList, out_stream: &mut W) -> Result<()> {
    use crate::result::writer::Writer;
    let w: &mut BoxedWriterFn =
      self
        .writer_fns
        .get_mut(&self.config.output_format)
        .ok_or(Error::Custom(format!(
          "{}: unknown output format",
          self.config.output_format
        )))?;
    w(Box::new(out_stream), res)?;
    out_stream
      .write("\n".as_bytes())
      .or_else(|e| Err(Error::IO(e.to_string())))?;
    Ok(())
  }

  fn prepare_after_help(&self) -> Result<String> {
    use std::io::Write;
    let mut buf: Vec<u8> = vec![];
    write!(buf, "\nAVAILABLE GENERATORS\n");
    self.dump_generators(&mut buf)?;
    write!(buf, "\n  (use --list-generators to get the full list)\n");
    write!(buf, "\nAVAILABLE FORMATS\n");
    self.dump_formats(&mut buf)?;
    Ok(String::from(std::str::from_utf8(&buf).expect("failed to convert utf8 string")))
  }

  fn parse_options(&mut self) -> Result<()> {
    let dflt_timeout = match self.config.timeout {
      None => "none".to_string(),
      Some(t) => format!("{:?}", t.as_secs_f32()),
    };
    let after_help = self.prepare_after_help()?;
    let matches = CliApp::new(env!("CARGO_PKG_NAME"))
      .version(env!("CARGO_PKG_VERSION"))
      .author(env!("CARGO_PKG_AUTHORS"))
      .about("Generate dictionnary for mock values, pentesting etc...")
      .arg(
        Arg::with_name("dump data")
          .long("dump-data")
          .help("Print data to stdout and exit"),
      )
      .arg(
        Arg::with_name("verbose")
          .long("verbose")
          .takes_value(true)
          .default_value("low")
          .possible_values(&["none", "low", "medium", "high"])
          .help("Show additionnal log messages"),
      )
      .arg(
        Arg::with_name("limit")
          .short("L")
          .long("limit")
          .value_name("LINE_COUNT")
          .help("Limit the number of generated lines")
          .takes_value(true),
      )
      // .arg(
      //   Arg::with_name("data-source")
      //     .short("D")
      //     .long("data-source")
      //     .value_name("PATH")
      //     .help("Define the data source to use")
      //     .takes_value(true),
      // )
      // TODO Implement this
      .arg(
        Arg::with_name("locale")
          .long("locale")
          .value_name(DEFAULT_LOCALE)
          .help("Override system locale")
          .takes_value(true),
      )
      .arg(
        Arg::with_name("format")
          .short("f")
          .long("format")
          .value_name("EXT")
          .default_value("csv")
          .help("Define output format")
          .takes_value(true),
      )
      .arg(
        Arg::with_name("list-formats")
          .long("list-formats")
          .help("List available output formats"),
      )
      .arg(
        Arg::with_name("list-generators")
          .long("list-generators")
          .help("List available data generators"),
      )
      .arg(
        Arg::with_name("generator")
          .short("g")
          .long("generator")
          .value_name("NAME")
          .help("Set the generator to use")
          .takes_value(true),
      )
      .arg(
        Arg::with_name("timeout")
          .short("t")
          .long("timeout")
          .default_value(dflt_timeout.as_str())
          .takes_value(true)
          .help("Define the maximum time spent generating (in seconds, no suffix)"),
      )
      .arg(
        Arg::with_name("minimum")
          .short("m")
          .long("min")
          .value_name("MIN_LENGTH")
          .help("Set the minimum length of the words to generate")
          .takes_value(true),
      )
      .arg(
        Arg::with_name("maximum")
          .short("M")
          .long("max")
          .value_name("MAX_LENGTH")
          .help("Set the maximum length of the words to generate")
          .takes_value(true),
      )
      .arg(
        Arg::with_name("interactive")
          .help("Interactively ask configuration values")
          .short("i")
          .long("interactive")
          .takes_value(false),
      )
      .after_help(after_help.as_str())
      .get_matches();

    if matches.is_present("verbose") {
      let vs = matches.value_of("verbose").unwrap();
      self.config.verbosity = match Verbosity::parse(vs) {
        None => return Err(Error::InvalidOption{name: "verbose level".to_string(), message: "allowed values: none, low, medium, high".to_string()}),
        Some(v) => v,
      };
    }

    if matches.is_present("timeout") {
      self.config.timeout = match matches.value_of("timeout").unwrap() {
        ""|"none" => None,
        val => {
          match val.parse::<f32>() {
            Ok(v) => Some(std::time::Duration::from_secs_f32(v)),
            Err(e) => return Err(Error::InvalidOption{name: "timeout".to_string(), message: e.to_string()})
          }
        }
      };
    }

    if matches.is_present("format") {
      let v = matches.value_of("format").unwrap();
      let w = self.writers.iter().find(|w| w.name() == v || w.ext() == v);
      if w.is_none() {
        return Err(Error::UnknownFormat(v.to_string()));
      }
      self.config.output_format = v.to_string();
    }
    
    // let yaml = load_yaml!("cli.yml");
    // let matches = App::from_yaml(yaml).get_matches();
    self.config.interactive = matches.is_present("interactive");
    self.config.min_length = match matches.value_of("minimum") {
      Some(v) => v
        .parse::<i64>()
        .or_else(|e| Err(Error::Custom(e.to_string())))?,
      None => self.config.min_length,
    };
    self.config.max_length = match matches.value_of("maximum") {
      Some(v) => v
        .parse::<i64>()
        .or_else(|e| Err(Error::Custom(e.to_string())))?,
      None => self.config.max_length,
    };
    self.config.limit = match matches.value_of("limit") {
      Some(v) => v
        .parse::<i64>()
        .or_else(|e| Err(Error::Custom(e.to_string())))?,
      None => self.config.limit,
    };
    self.config.locale = match matches.value_of("locale") {
      Some(v) => v.to_string(),
      None => self.config.locale.to_string(),
    };
    self.config.generator = matches
      .value_of("generator")
      .unwrap_or(self.config.generator.as_str())
      .to_string();
    self.config.dump_data = matches.is_present("dump data");

    if matches.is_present("list-formats") {
      self.dump_formats(&mut std::io::stdout())?;
      return Err(Error::SilentExit);
    }

    if matches.is_present("list-generators") {
      self.setup_sources()?;
      self.dump_generators(&mut std::io::stdout())?;
      return Err(Error::SilentExit);
    }
    
    Ok(())
  }

  fn dump_formats<W: std::io::Write>(&self, w: &mut W) -> Result<()> {
    let fmt_one = |name, ext| format!(" - {} - {}", name, ext);
    let mut fmt_buf: Vec<String> = self
      .writers
      .iter()
      .map(|g| fmt_one(g.name(), g.ext()))
      .collect();
    write!(w, "{}", fmt_buf.iter().fold(String::new(), |accu, line| format!("{}{}\n", accu, line)))
      .or_else(|e| Err(Error::IO(e.to_string())))?;
    Ok(())
  }

  fn dump_generators<W: std::io::Write>(&self, w: &mut W) -> Result<()> {
    let fmt_one = |name| format!(" - {}", name);
    let mut fmt_buf: Vec<String> = self
      .generators
      .iter()
      .map(|g| fmt_one(g.name()))
      .collect();
    write!(w, "{}", fmt_buf.iter().fold(String::new(), |accu, line| format!("{}{}\n", accu, line)));
    Ok(())
  }

  fn configure(&mut self) -> Result<()> {
    info!("{} generators", style("Configuring").green().bold());
    self.select_generator()?;
    self.configure_limit()?;
    self.configure_lens()?;
    Ok(())
  }

  /// Configure the min/max generation lengths
  fn configure_lens(&mut self) -> Result<()> {
    loop {
      self
        .terminal
        .write_str("  * Length (min x max): ")
        .or_else(|e| Err(Error::IO(e.to_string())))?;
      let text = format!("{}x{}", self.config.min_length, self.config.max_length);
      let length = self
        .terminal
        .read_line_initial_text(text.as_str())
        .or_else(|e| Err(Error::IO(e.to_string())))?;
      self
        .terminal
        .write_line("")
        .or_else(|e| Err(Error::IO(e.to_string())))?;
      let parts: Vec<String> = length.split("x").map(|l| l.to_string()).collect();
      if parts.len() != 2 {
        self
          .terminal
          .write_line("format: min x max")
          .or_else(|e| Err(Error::IO(e.to_string())))?;
      }
      let min_res = parts[0].parse::<i64>();
      let mut err: Option<Error> = None;
      if min_res.is_err() {
        err = Some(Error::Custom(format!(
          "Failed to parse min length '{}', {}",
          parts[0],
          min_res.unwrap_err()
        )));
      } else {
        self.config.min_length = min_res.unwrap();
        let max_res = parts[1].parse::<i64>();
        if max_res.is_err() {
          err = Some(Error::Custom(format!(
            "Failed to parse max length '{}', {}",
            parts[1],
            max_res.unwrap_err()
          )));
        } else {
          self.config.max_length = max_res.unwrap();
        }
      }
      match err {
        Some(e) => {
          let text = format!("{}", e.to_string());
          self
            .terminal
            .write_line(text.as_str())
            .or_else(|e| Err(Error::IO(e.to_string())))?;
        }
        None => {
          break;
        }
      }
    }
    Ok(())
  }

  /// Configure the min/max generation lengths
  fn configure_limit(&mut self) -> Result<()> {
    loop {
      self
        .terminal
        .write_str("  * Limit: ")
        .or_else(|e| Err(Error::IO(e.to_string())))?;
      let text = format!("{}", self.config.limit);
      let limit = self
        .terminal
        .read_line_initial_text(text.as_str())
        .or_else(|e| Err(Error::IO(e.to_string())))?;
      self
        .terminal
        .write_line("")
        .or_else(|e| Err(Error::IO(e.to_string())))?;
      let res = limit.parse::<i64>();
      if res.is_err() {
        let err = Error::Custom(format!(
          "Failed to parse min limit '{}', {}",
          limit,
          res.unwrap_err()
        ));
        let text = format!("{}", err.to_string());
        self
          .terminal
          .write_line(text.as_str())
          .or_else(|e| Err(Error::IO(e.to_string())))?;
      } else {
        self.config.limit = res.unwrap();
        break;
      }
    }
    Ok(())
  }

  fn select_generator(&mut self) -> Result<()> {
    let mut line: String;
    let mut first_gen = String::new();
    eprintln!(
      "Available:\n{}",
      self
        .generators
        .iter()
        .map(|g| format!(" - {}", g.name()))
        .fold(String::new(), |accu, line| format!("{}\n{}", accu, line))
    );
    if let Some(g) = self.generators.first() {
      first_gen = g.name().clone();
    }
    let mut selected = String::new();
    let is_known = |name: &String| {
      self
        .generators
        .iter()
        .find(|g| *g.name() == *name)
        .is_some()
    };
    while !is_known(&selected) {
      self
        .terminal
        .write_str("  * Generator: ")
        .or_else(|e| Err(Error::IO(e.to_string())))?;
      selected = self
        .terminal
        .read_line_initial_text(first_gen.as_str())
        .or_else(|e| Err(Error::IO(e.to_string())))?;
      self
        .terminal
        .write_line("")
        .or_else(|e| Err(Error::IO(e.to_string())))?;
    }
    self.config.generator = selected;
    Ok(())
  }

  pub fn setup_locale(&mut self) -> Result<&'static locale_codes::language::LanguageInfo> {
    let mut user_locale = self.config.locale.to_string();
    if user_locale.is_empty() {
      user_locale = format!("{}", locale_config::Locale::user_default());
    }
    let parts: Vec<&str> = user_locale.split("-").collect();
    let loc = parts.get(0).unwrap();
    set_locale(loc)?;
    let ret = get_locale();
    self.config.locale = ret.terminology_code.clone().unwrap_or(loc.to_string());
    info!("set locale to '{}'", self.config.locale);
    Ok(get_locale())
  }


  fn sequence_datasets(&self) -> Vec<Dataset> {
    sources()
      .iter()
      .fold(vec![], |mut accu, source| {
        source.datasets().iter().filter(|ds| ds.typ == "sequence").for_each(|ds| accu.push(ds.clone()));
        accu 
      })
  }

  fn sequence_generator(&self, name: &String) -> Option<BoxedGenerator> {
    let sets = self.sequence_datasets();
    let ds = sets.iter().find(|ds| ds.name == *name);
    if ds.is_none() {
      return None;
    }
    let g: BoxedGenerator = Box::new(
      SequenceGenerator::with_config(Some(name.clone()), &self.config, ds.unwrap().attribs.clone())
        .expect("invalid sequence declaration"),
    );
    Some(g)
  }
}
