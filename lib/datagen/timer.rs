use std::time::Instant;
use std::time::Duration;
use std::collections::HashMap;

pub struct Timer {
  pub start: Instant,
  pub steps: HashMap<String, Instant>,
  pub last_step: Option<String>
}

impl Timer {
  pub fn new() -> Timer {
    Timer {
      start: Instant::now(),
      steps: HashMap::new(),
      last_step: None,
    }
  }

  pub fn reset(&mut self) -> &mut Timer {
    self.start = Instant::now();
    self.steps = HashMap::new();
    self.last_step = None;
    self
  }

  pub fn total_elapsed(&self) -> Duration {
    Instant::now().duration_since(self.start)
  }

  pub fn elapsed_since_step<S: AsRef<str>>(&self, s: S) -> Option<Duration> {
    let step = self.steps.iter().find(|(k, v)| k.as_str() == s.as_ref());
    match step {
      None => None,
      Some((k, v)) => Some(Instant::now().duration_since(*v))
    }
  }

  pub fn steps(&self) -> &HashMap<String, Instant> {
    &self.steps
  }

  pub fn step<S: AsRef<str>>(&mut self, n: S) -> Duration {
    let last_step = self.last_step.clone();
    self.last_step = Some(n.as_ref().to_string());
    self.steps.insert(self.last_step.as_ref().unwrap().clone(), Instant::now());
    match last_step {
      None => Duration::from_secs(0),
      Some(name) => Instant::now().duration_since(*self.steps.get(&name).unwrap())
    }
  }
}