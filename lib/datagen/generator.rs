use super::config::{Config, GENERATOR_TIMEOUT_SECONDS};
use super::data::BoxedSource;
use super::result::Result;
use crate::result::app_result::{AppResult, AppResultList};
use crate::timer::Timer;

// The generator trait generate data on the fly.
// Most of the times it will take input from data sources
// and comine it in some way.
pub trait Generator {
  fn name(&self) -> &String;

  // Configure the generator once app options are parsed
  fn configure(&mut self, conf: &Config) -> Result<()>;
  
  // Retrieve the config
  fn config(&self) -> &Config;

  // Load data from external sources
  fn load_source(&mut self, s: &BoxedSource) -> Result<()>;

  // Generate a single item
  fn generate_one(&mut self) -> Result<AppResult>;

  // Generate all items, as defined by user config
  fn generate(&mut self) -> Result<AppResultList> {
    let mut ret: AppResultList = vec![];
    let mut limit = self.config().limit;
    let mut timer = Timer::new();
    if limit < 0 {
      limit = 1000000;
    }
    let mut generated: AppResult = AppResult::None;
    let mut num_dups: i32;
    let timeout = self.config().timeout;
    let mut timeout_reached = false;
    let mut i = 0;
    // until limit or timeout reached
    while i < limit && !timeout_reached {
      // generate item
      generated = self.generate_one()?;
      // check for duplicates
      while ret
        .iter()
        .find(|w| **w == generated)
        .clone()
        .is_some()
        && !timeout_reached
      {
        generated = self.generate_one()?;
        if let Some(t) = timeout {
          timeout_reached = timer.total_elapsed().as_secs_f32() >= t.as_secs_f32();
        }
      }
      if !ret
        .iter()
        .find(|w| **w == generated)
        .clone()
        .is_some()
      {
        // add to results if no duplicates found
        ret.push(generated.clone());
      }
      if let Some(t) = timeout {
        timeout_reached = timer.total_elapsed().as_secs_f32() >= t.as_secs_f32();
      }
      i += 1;
    }
    if limit as usize > ret.len() {
      // warn something went bad
      if timeout_reached {
        log::warn!("timeout reached: try increasing the delay by using --timeout");
      }
      log::info!("only {}/{} items produced, either:", ret.len(), limit);
      log::info!("- there is not enough data in the database");
      log::info!("- there was too much collisions");
      log::info!("- the timeout was reached");
      log::info!("if the database has enough data, try increasing the timeout");
    }
    Ok(ret)
  }
}

pub type BoxedGenerator = Box<dyn Generator>;
pub type BoxedGeneratorList = Vec<BoxedGenerator>;
