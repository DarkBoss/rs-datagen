use rusqlite::{Connection};

use std::collections::HashMap;

use crate::result::Result;

pub type Value = Option<String>;
pub type Row = HashMap<String, Value>;
pub type Rows = Vec<Row>;

pub type WriteQuerySuccess = Vec<i64>;
pub type ReadQuerySuccess = Rows;

pub type WriteQueryResult = Result<WriteQuerySuccess>;
pub type ReadQueryResult = Result<ReadQuerySuccess>;

use std::cmp::Eq;
use std::hash::Hash;

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub enum CmpOp {
  Equ,
  Nequ,
  GreaterEqu,
  LessEqu,
  Greater,
  Less,
  In,
  Like
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub enum Op {
  Or,
  And
}

#[derive(Clone, Debug)]
pub enum Expr {
  Field(Op, String, CmpOp, Value),
  Sub(Op, Vec<Expr>),
}

pub fn cmp_op_symbols() -> HashMap<CmpOp, String> {
  let mut map = HashMap::<CmpOp, String>::new();
  map.insert(CmpOp::Equ, "=".into());
  map.insert(CmpOp::Nequ, "!=".into());
  map.insert(CmpOp::GreaterEqu, "<=".into());
  map.insert(CmpOp::LessEqu, "<=".into());
  map.insert(CmpOp::Greater, ">".into());
  map.insert(CmpOp::Less, "<".into());
  map.insert(CmpOp::In, "in".into());
  map.insert(CmpOp::Like, "like".into());
  map
}

impl std::fmt::Display for CmpOp {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{}", cmp_op_symbols().get(self).unwrap())
  }
}

pub fn op_symbols() -> HashMap<Op, String> {
  let mut map = HashMap::<Op, String>::new();
  map.insert(Op::And, "and".into());
  map.insert(Op::Or, "or".into());
  map
}

impl std::fmt::Display for Op {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{}", op_symbols().get(self).unwrap())
  }
}

pub struct Query {
  table: String,
  exprs: Vec<Expr>,
  query: String,
}

impl Query {
  pub fn new(table: String) -> Query {
    Query {
      table: table,
      exprs: Vec::new(),
      query: String::new()
    }
  }

  pub fn or<'a>(&'a mut self, expr: Expr) -> &'a mut Self {
    self.exprs.push(expr);
    self
  }

  pub fn and<'a>(&'a mut self, expr: Expr) -> &'a mut Self {
    self.exprs.push(expr);
    self
  }

  fn build_field_expr(op: Op, field: String, cmp: CmpOp, val: Value) -> String {
    format!("{} {} {} {}", op, field, cmp, val.clone().map_or(String::from("NULL"), |v| v.to_string()))
  }
  
  fn build_sub_expr(op: Op, exprs: Vec<Expr>) -> String {
    let mut ret = String::new();
    for expr in exprs {
      ret = format!("{} {}", ret, Query::build_expr(expr));
    }
    ret
  }

  fn build_expr(expr: Expr) -> String {
    match expr {
      Expr::Field(op, field, cmp, val) => Query::build_field_expr(op, field, cmp, val),
      Expr::Sub(op, exprs) => Query::build_sub_expr(op, exprs)
    }
  }

  pub fn build(&mut self) -> &String {
    self.query.clear();
    self.exprs.iter()
              .map(move |expr| Query::build_expr(expr.clone()))
              .collect::<Vec<String>>();
    &self.query
  }

  pub fn execute(conn: &Connection) {}
}
