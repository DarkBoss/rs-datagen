use super::query::{Query, ReadQueryResult, Row, Rows, WriteQueryResult};
use super::{Data, Dataset, Domain, Locale};
use crate::config::DEFAULT_LOCALE;
use crate::error::Error;
use crate::result::Result;
use crate::result::{AppResult, AppResultList, CSVWriter, Writer};
use std::io::BufWriter;
use std::io::Write;

use std::collections::HashMap;

#[derive(Copy, Clone, Debug, Hash, Eq, PartialEq)]
pub enum SupportedType {
  Sqlite,
  Mock,
}

impl std::default::Default for SupportedType {
  fn default() -> Self {
    Self::Sqlite
  }
}

impl std::fmt::Display for SupportedType {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    let found: Vec<String> = Self::all()
      .iter()
      .filter(|&(k, _)| *k == *self)
      .map(|(_, v)| v.clone())
      .collect::<Vec<String>>();
    let deflt = "unknown".to_string();
    let name = found.first().take().unwrap_or(&deflt);
    write!(f, "{}", name)
  }
}

impl SupportedType {
  pub fn all() -> std::collections::HashMap<SupportedType, String> {
    let mut d = std::collections::HashMap::<SupportedType, String>::new();
    d.insert(SupportedType::Sqlite, "sqlite".to_string());
    d
  }

  pub fn parse(s: &String) -> Result<SupportedType> {
    if *s == format!("{}", SupportedType::Sqlite) {
      return Ok(SupportedType::Sqlite);
    }
    Err(Error::DataSourceError(format!(
      "Unknown data source type: {}",
      s
    )))
  }
}

#[derive(Clone, Debug)]
pub struct SourceConfig {
  pub typ: SupportedType,
  pub url: String,
  pub open_now: bool,
  pub locale: String,
}

impl std::default::Default for SourceConfig {
  fn default() -> SourceConfig {
    SourceConfig {
      typ: SupportedType::default(),
      url: String::new(),
      open_now: false,
      locale: String::from(DEFAULT_LOCALE),
    }
  }
}

impl SourceConfig {
  pub fn with_values(
    url: String,
    open_now: bool,
    locale: String,
    typ: SupportedType,
  ) -> SourceConfig {
    SourceConfig {
      typ,
      url,
      open_now,
      locale,
    }
  }
}

pub trait Source {
  fn name(&self) -> String;

  fn open(&mut self, cfg: SourceConfig) -> Result<()>;
  fn close(self) -> Result<()>;

  fn load(&mut self) -> Result<()>;

  fn next_domain_id(&self) -> i64 {
    self.domains().iter().fold(1, |accu, item| match item {
      _ if item.id > accu => item.id,
      v => accu,
    })
  }
  fn next_locale_id(&self) -> i64 {
    self.locales().iter().fold(1, |accu, item| match item {
      _ if item.id > accu => item.id,
      v => accu,
    })
  }
  fn next_dataset_id(&self) -> i64 {
    self.datasets().iter().fold(1, |accu, item| match item {
      _ if item.id > accu => item.id,
      v => accu,
    })
  }
  fn next_data_id(&self) -> i64 {
    self.datas().iter().fold(1, |accu, item| match item {
      _ if item.id > accu => item.id,
      v => accu,
    })
  }
  fn add_domain(&mut self, mut d: Domain) -> Result<&mut Domain> {
    d.id = self.next_domain_id();
    let mut arr = self.domains_mut();
    arr.push(d);
    Ok(arr.last_mut().unwrap())
  }
  fn add_locale(&mut self, mut l: Locale) -> Result<&mut Locale> {
    l.id = self.next_locale_id();
    let mut arr = self.locales_mut();
    arr.push(l);
    Ok(arr.last_mut().unwrap())
  }
  fn add_dataset(&mut self, mut d: Dataset) -> Result<&mut Dataset> {
    d.id = self.next_dataset_id();
    let mut arr = self.datasets_mut();
    arr.push(d);
    Ok(arr.last_mut().unwrap())
  }

  fn add_data(&mut self, mut d: Data) -> Result<&mut Data> {
    d.id = self.next_data_id();
    let mut arr = self.datas_mut();
    arr.push(d);
    Ok(arr.last_mut().unwrap())
  }
  fn domains(&self) -> &Vec<Domain>;
  fn locales(&self) -> &Vec<Locale>;
  fn datasets(&self) -> &Vec<Dataset>;
  fn datas(&self) -> &Vec<Data>;

  fn domains_mut(&mut self) -> &mut Vec<Domain>;
  fn locales_mut(&mut self) -> &mut Vec<Locale>;
  fn datasets_mut(&mut self) -> &mut Vec<Dataset>;
  fn datas_mut(&mut self) -> &mut Vec<Data>;
}

pub fn dump_source<S: Source + ?Sized, W: std::io::Write>(src: &Box<S>, onto: &mut W) -> Result<()> {
  let mut w = CSVWriter::new();
  let mut res = AppResultList::new();
  *w.header_mut() = vec![
    "type", "id", "locale", "domain", "dataset", "dataset_type", "name", "value"
  ].iter()
    .map(|c| c.to_string())
    .collect();
  for domain in src.domains() {
    let mut map = HashMap::<String, String>::new();
    map.insert("type".to_string(), "domain".to_string());
    map.insert("id".to_string(), format!("{}", domain.id));
    map.insert("locale".to_string(), "".to_string());
    map.insert("domain".to_string(), "".to_string());
    map.insert("dataset".to_string(), "".to_string());
    map.insert("dataset_type".to_string(), "".to_string());
    map.insert("name".to_string(), domain.name.clone());
    map.insert("value".to_string(), "".to_string());
    res.push(AppResult::Complex(map));
  }
  for locale in src.locales() {
    let mut map = HashMap::<String, String>::new();
    map.insert("type".to_string(), "locale".to_string());
    map.insert("id".to_string(), format!("{}", locale.id));
    map.insert("locale".to_string(), "".to_string());
    map.insert("domain".to_string(), "".to_string());
    map.insert("dataset".to_string(), "".to_string());
    map.insert("dataset_type".to_string(), "".to_string());
    map.insert("name".to_string(), locale.name.clone());
    map.insert("value".to_string(), "".to_string());
    res.push(AppResult::Complex(map));
  }
  for dataset in src.datasets() {
    let mut map = HashMap::<String, String>::new();
    map.insert("type".to_string(), "dataset".to_string());
    map.insert("id".to_string(), format!("{}", dataset.id));
    map.insert("locale".to_string(), "".to_string());
    map.insert("domain".to_string(), format!("{}", dataset.domain));
    map.insert("dataset".to_string(), "".to_string());
    map.insert("dataset_type".to_string(), format!("{}", dataset.typ));
    map.insert("name".to_string(), dataset.name.clone());
    map.insert("value".to_string(), "".to_string());
    res.push(AppResult::Complex(map));
  }
  for data in src.datas() {
    let mut map = HashMap::<String, String>::new();
    map.insert("type".to_string(), "data".to_string());
    map.insert("id".to_string(), format!("{}", data.id));
    map.insert("locale".to_string(), match data.locale {
      Some(l) => format!("{}", l),
      None => "NULL".to_string(),
    });
    map.insert("domain".to_string(), "".to_string());
    map.insert("dataset".to_string(), format!("{}", data.dataset));
    map.insert("dataset_type".to_string(), "".to_string());
    map.insert("name".to_string(), data.name.clone());
    map.insert("value".to_string(), data.value.clone());
    res.push(AppResult::Complex(map));
  }
  w.write::<W>(onto, &res)
    .or_else(|e| Err(Error::IO(e.to_string())))?;
  Ok(())
}

pub type BoxedSource = Box<dyn Source>;
pub type BoxedSourceList = Vec<BoxedSource>;
