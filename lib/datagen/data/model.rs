#[derive(Debug, Default, Clone)]
pub struct Locale {
  pub id: i64,
  pub name: String,
}

impl Locale {
  pub fn new (id: i64, name: String) -> Locale {
    Locale {
      id,
      name
    }
  }
}

#[derive(Debug, Default, Clone)]
pub struct Domain {
  pub id: i64,
  pub name: String,
}

impl Domain {
  pub fn new (id: i64, name: String) -> Domain {
    Domain {
      id,
      name
    }
  }
}
#[derive(Debug, Default, Clone)]
pub struct Dataset {
  pub id: i64,
  pub domain: i64,
  pub name: String,
  pub typ: String,
  pub attribs: String,
}

impl Dataset {
  pub fn new (id: i64, domain: i64, name: String, typ: String, attribs: String) -> Dataset {
    Dataset {
      id,
      domain,
      name,
      typ,
      attribs
    }
  }
}

#[derive(Debug, Default, Clone)]
pub struct Data {
  pub id: i64,
  pub dataset: i64,
  pub locale: Option<i64>,
  pub name: String,
  pub value: String,
}

impl Data {
  pub fn new (id: i64, dataset: i64, locale: Option<i64>, name: String, value: String) -> Data {
    Data {
      id,
      dataset,
      locale,
      name,
      value
    }
  }
}