use rusqlite::NO_PARAMS;
use crate::error::Error;
use crate::result::Result;
use super::{Domain, Locale, Dataset, Data};

use super::source::*;
use super::query::*;

use rusqlite::{Connection, params};

use log::{info, trace, error, warn};

const LOCALE_SELECTION_QUERY: &'static str = "select `id`, `name` from locale";
const DOMAIN_SELECTION_QUERY: &'static str = "select `id`, `name` from domain;";
const DATASET_SELECTION_QUERY: &'static str = "select `id`, `domain`, `name`, `type`, `attribs` from dataset";

fn build_data_query(locale: String) -> String {
  format!("select `id`, `dataset`, `locale`, `name`, `value` from data where locale is NULL or locale in (select id from locale where name = '{}')", locale)
}


pub struct SQLiteSource {
  cfg: SourceConfig,
  conn: Option<Box<Connection>>,
  locales: Vec<Locale>,
  domains: Vec<Domain>,
  datasets: Vec<Dataset>,
  datas: Vec<Data>
}

impl SQLiteSource {
  pub fn open(cfg: SourceConfig) -> Result<SQLiteSource> {
    let mut source = SQLiteSource {
      cfg: SourceConfig::default(),
      conn: None,
      locales: vec![],
      domains: vec![],
      datasets: vec![],
      datas: vec![]
    };
    source.open(cfg)?;
    Ok(source)
  }

  pub fn config(&self) -> &SourceConfig {
    &self.cfg
  }

  pub fn config_mut(&mut self) -> &mut SourceConfig {
    &mut self.cfg
  }

  pub fn url(&self) -> &String {
    &self.cfg.url
  }

  pub fn url_mut(&mut self) -> &mut String {
    &mut self.cfg.url
  }


  fn load_datas(&mut self) -> Result<&Vec<Data>> {
    let ret: Vec<Data> = vec![];
    match self.conn {
      Some(ref c) => {
        let q = build_data_query(self.cfg.locale.to_string());
        let mut stmt = c.prepare(&q).or_else(|e| Err(Error::DataSourceError(e.to_string())))?;
        let datas = stmt.query_map(params![], |row| {
            Ok(Data {
                id: row.get(0)?,
                dataset: row.get(1)?,
                locale: row.get(2)?,
                name: row.get(3)?,
                value: row.get(4)?
            })
        }).or_else(|e| Err(Error::DataSourceError(e.to_string())))?;
        for data in datas {
          self.datas.push(data.unwrap());
        }
        if self.datas.len() == 0 {
          warn!("no data found for locale '{}'", self.cfg.locale);
        }
        Ok(&self.datas)
      },
      None => Err(Error::DataSourceError("connection currently closed".to_string())),
    }
  }
  
  fn load_domains(&mut self) -> Result<Vec<Domain>> {
    let ret: Vec<Domain> = vec![];
    match self.conn {
      Some(ref c) => {
        let mut stmt = c.prepare(DOMAIN_SELECTION_QUERY).or_else(|e| Err(Error::DataSourceError(e.to_string())))?;
        let domains = stmt.query_map(params![], |row| {
            Ok(Domain {
                id: row.get(0)?,
                name: row.get(1)?,
            })
        }).or_else(|e| Err(Error::DataSourceError(e.to_string())))?;

        for domain in domains {
          self.domains.push(domain.unwrap());
        }
        Ok(())
      },
      None => Err(Error::DataSourceError("connection currently closed".to_string())),
    }?;
    Ok(ret)
  }

  fn load_locales(&mut self) -> Result<Vec<Locale>> {
    let ret: Vec<Locale> = vec![];
    match self.conn {
      Some(ref c) => {
        let mut stmt = c.prepare(LOCALE_SELECTION_QUERY).or_else(|e| Err(Error::DataSourceError(e.to_string())))?;
        let locales = stmt.query_map(params![], |row| {
            Ok(Locale {
                id: row.get(0)?,
                name: row.get(1)?,
            })
        }).or_else(|e| Err(Error::DataSourceError(e.to_string())))?;

        for locale in locales {
          self.locales.push(locale.unwrap());
        }
        Ok(())
      },
      None => Err(Error::DataSourceError("connection currently closed".to_string())),
    }?;
    Ok(ret)
  }

  fn load_datasets(&mut self) -> Result<Vec<Dataset>> {
    let ret: Vec<Dataset> = vec![];
    match self.conn {
      Some(ref c) => {
        let mut stmt = c.prepare(DATASET_SELECTION_QUERY).or_else(|e| Err(Error::DataSourceError(e.to_string())))?;
        let datasets = stmt.query_map(params![], |row| {
          let values: (i64, i64, String, String, String) = (row.get(0)?, row.get(1)?, row.get(2)?, row.get(3)?, row.get(4)?);
            Ok(Dataset {
                id: values.0,
                domain: values.1,
                name: values.2,
                typ: values.3,
                attribs: values.4,
            })
        }).or_else(|e| Err(Error::DataSourceError(e.to_string())))?;

        for data in datasets {
          self.datasets.push(data.unwrap());
        }
        Ok(())
      },
      None => Err(Error::DataSourceError("connection currently closed".to_string())),
    }?;
    Ok(ret)
  }
  
}

impl Source for SQLiteSource {
  fn open(&mut self, cfg: SourceConfig) -> Result<()> {
    self.cfg = cfg;
    self.conn = Some(Box::new(Connection::open(self.url().clone()).or_else(
      |e| {
        Err(Error::DataSourceError(format!(
          "{}: {}",
          self.url(),
          e.to_string()
        )))
      },
    )?));
    // unsafe {
    //   rusqlite::trace::config_log(Some(|code: std::os::raw::c_int, msg: &str| {
    //     trace!("[SQLite:{}] {}", code, msg);
    //   })).or_else(|e| Err(Error::Custom(e.to_string())))?;
    // }
    trace!("opened connection to '{}'", self.url());
    Ok(())
  }

  fn close(mut self) -> Result<()> {
    if let Some(c) = self.conn {
      c.close()
        .or_else(|(_, e)| Err(Error::DataSourceError(e.to_string())))?;
    }
    self.conn = None;
    Ok(())
  }
  
  fn name(&self) -> String {
    String::from("SQLite")
  }

  fn load(&mut self) -> Result<()> {
    self.load_locales()?;
    self.load_domains()?;
    self.load_datasets()?;
    self.load_datas()?;
    Ok(())
  }

  fn domains(&self) -> &Vec<Domain> {
    &self.domains
  }
  fn locales(&self) -> &Vec<Locale> {
    &self.locales
  }
  fn datasets(&self) -> &Vec<Dataset> {
    &self.datasets
  }
  fn datas(&self) -> &Vec<Data> {
    &self.datas
  }

  fn domains_mut(&mut self) -> &mut Vec<Domain> {
    &mut self.domains
  }
  fn locales_mut(&mut self) -> &mut Vec<Locale> {
    &mut self.locales
  }
  fn datasets_mut(&mut self) -> &mut Vec<Dataset> {
    &mut self.datasets
  }
  fn datas_mut(&mut self) -> &mut Vec<Data> {
    &mut self.datas
  }
  
}