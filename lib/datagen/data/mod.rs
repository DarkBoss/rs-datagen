mod query;
mod source;
mod sqlite;
pub mod mock;
pub mod model;

use crate::config::Config;
pub use query::*;
pub use source::*;
pub use sqlite::*;
pub use model::*;
use crate::error::Error;
use crate::result::Result;

static mut g_sources: Option<Vec<BoxedSource>> = None;

pub fn sources() -> &'static Vec<BoxedSource> {
  let ret: &'static Vec<BoxedSource>;
  unsafe {
    if g_sources.is_none() {
      panic!("data_sources not allocated!");
    }
    ret = g_sources.as_ref().unwrap();
  }
  ret
}

pub fn sources_mut() -> &'static mut Vec<BoxedSource> {
  let ret: &'static mut Vec<BoxedSource>;
  unsafe {
    if g_sources.is_none() {
      panic!("data_sources not allocated!");
    }
    ret = g_sources.as_mut().unwrap();
  }
  ret
}

pub fn setup_sources(app: &Config) -> Result<&'static Vec<BoxedSource>> {
  use log::trace;
  unsafe {
    g_sources = Some(vec![]);
    let data_sources: &Vec<SourceConfig> = &app.data_sources;
    for data_source in data_sources {
      let mut cfg = data_source.clone();
      cfg.locale = app.locale.clone();
      trace!("Registering data source '{}' - {:?}", data_source.url, data_source);
      let mut source: BoxedSource = match cfg.typ {
        SupportedType::Sqlite => Box::new(SQLiteSource::open(cfg)?),
        SupportedType::Mock => Box::new(mock::MockSource::open(cfg)?),
        t => return Err(Error::DataSourceError(format!("unsupported type '{}'", t)))
      };
      source.load()?;
      sources_mut().push(source);
    }
    Ok(sources())
  }
}

pub fn known_data_sources() -> &'static Vec<BoxedSource> {
  unsafe {
    if g_sources.is_none() {
      panic!("data_sources not allocated!");
    }
    g_sources.as_ref().unwrap()
  }
}
