use crate::data::source::Source;
use crate::data::source::dump_source;
use crate::data::source::SourceConfig;
use crate::error::Error;
use crate::result::Result;
use super::{Domain, Locale, Dataset, Data};

pub struct MockSource {
  cfg: SourceConfig,
  locales: Vec<Locale>,
  domains: Vec<Domain>,
  datasets: Vec<Dataset>,
  datas: Vec<Data>
}

impl MockSource {
  pub fn open(cfg: SourceConfig) -> Result<MockSource> {
    let mut source = MockSource {
      cfg: cfg.clone(),
      locales: vec![],
      domains: vec![],
      datasets: vec![],
      datas: vec![]
    };
    source.open(cfg.clone())?;
    Ok(source)
  }

  pub fn config(&self) -> &SourceConfig {
    &self.cfg
  }

  pub fn config_mut(&mut self) -> &mut SourceConfig {
    &mut self.cfg
  }

  pub fn url(&self) -> &String {
    &self.cfg.url
  }

  pub fn url_mut(&mut self) -> &mut String {
    &mut self.cfg.url
  }

}

impl Source for MockSource {
  fn open(&mut self, cfg: SourceConfig) -> Result<()> {
    self.cfg = cfg;
    log::trace!("opened test connection to '{}'", self.url());
    Ok(())
  }

  fn close(mut self) -> Result<()> {
    Ok(())
  }
  
  fn name(&self) -> String {
    String::from("Test")
  }

  fn load(&mut self) -> Result<()> {
    Ok(())
  }

  fn domains(&self) -> &Vec<Domain> {
    &self.domains
  }
  fn locales(&self) -> &Vec<Locale> {
    &self.locales
  }
  fn datasets(&self) -> &Vec<Dataset> {
    &self.datasets
  }
  fn datas(&self) -> &Vec<Data> {
    &self.datas
  }

  fn domains_mut(&mut self) -> &mut Vec<Domain> {
    &mut self.domains
  }
  fn locales_mut(&mut self) -> &mut Vec<Locale> {
    &mut self.locales
  }
  fn datasets_mut(&mut self) -> &mut Vec<Dataset> {
    &mut self.datasets
  }
  fn datas_mut(&mut self) -> &mut Vec<Data> {
    &mut self.datas
  }
  
}