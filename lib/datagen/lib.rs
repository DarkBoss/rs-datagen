extern crate log4rs;

extern crate locale_codes;
extern crate locale_config;

extern crate regex;

extern crate console;
extern crate rand;
extern crate rusqlite;
// extern crate fern;
extern crate log;

pub mod timer;
pub mod app;
pub mod config;
pub mod data;
pub mod error;
pub mod generator;
pub mod generators;
pub mod random_replace;
pub mod locale;
pub mod result;

use crate::data::mock::MockSource;
use crate::data::BoxedSource;
use crate::data::Source;
pub use app::*;
pub use config::*;
pub use data::SourceConfig;
pub use error::*;
pub use generator::*;
pub use generators::*;
pub use locale::*;
pub use result::*;
pub use result::*;
pub use random_replace::*;
pub use timer::*;
use std::collections::HashMap;

pub fn standard_generators() -> Vec<Box<dyn Generator>> {
  let mut ret: Vec<Box<dyn Generator>> = vec![];
  ret.push(Box::new(RandomPasswordGenerator::default()));
  ret.push(Box::new(PersonNameGenerator::default()));
  ret.push(Box::new(CountryNameGenerator::default()));
  ret.push(Box::new(CountryCodeGenerator::default()));
  ret.push(Box::new(PhoneNumberGenerator::default()));
  ret.push(Box::new(HealthSSNGenerator::default()));
  ret.push(Box::new(PersonGenerator::default()));
  return ret;
}

pub type WriterFn = dyn FnMut(Box<&mut dyn std::io::Write>, &AppResultList) -> Result<()>;
pub type BoxedWriterFn = Box<WriterFn>;
pub type WriterFnMap = HashMap<String, BoxedWriterFn>;

pub fn standard_writers() -> Vec<BoxedWriter<'static>> {
  vec![
    Box::new(CSVWriter::default()),
    Box::new(JSONWriter::default()),
  ]
}

pub fn standard_writer_fns() -> WriterFnMap {
  use std::iter::FromIterator;
  let funcs: Vec<(String, BoxedWriterFn)> = vec![
    (
      CSV_WRITER_EXT.to_string(),
      Box::new(|ref mut out_stream, ref res| -> Result<()> {
        let mut w = CSVWriter::default();
        w.write(out_stream, res)?;
        Ok(())
      }),
    ),
    (
      JSON_WRITER_EXT.to_string(),
      Box::new(|ref mut out_stream, ref res| -> Result<()> {
        let mut w = JSONWriter::default();
        w.write(out_stream, res)?;
        Ok(())
      }),
    ),
  ];
  WriterFnMap::from_iter(funcs.into_iter())
}

use log::{debug, error, info, trace, warn};
// use fern::InitError;
use std::path::Path;

pub fn setup_logger(level: log::LevelFilter) -> Result<()> {
  do_setup_logger("datagen", level)
}

pub fn setup_test_logger() -> Result<()> {
  do_setup_logger("datagen-tests", log::LevelFilter::max())
}

fn do_setup_logger<S: AsRef<str>>(output_name: S, level: log::LevelFilter) -> Result<()> {
  use log4rs::append::console::{Target, ConsoleAppender};
  use log4rs::append::file::FileAppender;
  use log4rs::append::rolling_file::policy::compound::roll::fixed_window::FixedWindowRoller;
  use log4rs::append::rolling_file::policy::compound::roll::Roll;
  use log4rs::append::rolling_file::policy::compound::trigger::size::SizeTrigger;
  use log4rs::append::rolling_file::policy::compound::CompoundPolicy;
  use log4rs::append::rolling_file::RollingFileAppender;
  use log4rs::config::{Appender, Root};
  use log4rs::encode::pattern::PatternEncoder;

  let filename = format!("log/{}.log", output_name.as_ref());
  let roller_format = format!("log/{}.{{}}.log", output_name.as_ref());
  let roller = FixedWindowRoller::builder()
    .base(1)
    .build(roller_format.as_str(), 5)
    .or_else(|e| Err(Error::Custom(e.to_string())))?;

  let policy = CompoundPolicy::new(Box::new(SizeTrigger::new(1000000)), Box::new(roller));

  let logger_app = RollingFileAppender::builder()
    .encoder(Box::new(PatternEncoder::new(
      "{d(%Y-%m-%d %H:%M:%S):<20.20} {h({l:<5.5})} {t:<20} {m}{n}",
    )))
    .build(format!("log/{}.log", output_name.as_ref()), Box::new(policy))
    .or_else(|e| Err(Error::Custom(e.to_string())))?;

  let logger_console = ConsoleAppender::builder()
  .target(Target::Stderr)
    .encoder(Box::new(PatternEncoder::new(
      "{d(%Y-%m-%d %H:%M:%S):<20.20} {h({l:<5.5})} {t:<20} {m}{n}",
    )))
    .build();

  let config = log4rs::config::Config::builder()
    .appender(Appender::builder().build("app", Box::new(logger_app)))
    .appender(Appender::builder().build("stdout", Box::new(logger_console)))
    .build(
      Root::builder()
        .appender("app")
        .appender("stdout")
        .build(level),
    )
    .or_else(|e| Err(Error::Custom(e.to_string())))?;
  log4rs::init_config(config).or_else(|e| Err(Error::Custom(e.to_string())))?;

  // log4rs::init_config(config).or_else(|e| {
  //   Err(Error::Custom(format!(
  //     "failed to setup logger: {}",
  //     e.to_string()
  //   )))
  // })?;
  Ok(())
  // log4rs::init_file(path, Default::default()).or_else(|e| {
  //   Err(Error::Custom(format!(
  //     "failed to setup logger: {}",
  //     e.to_string()
  //   )))
  // })
}

// static mut testing_logger_init: bool = false;

fn setup_testing(
  data_setup: Option<Box<FnOnce(&mut BoxedSource) -> Result<()>>>,
) -> Result<Config> {
  let cfg = Config {
    data_sources: vec![],
    generator: String::from(""),
    min_length: DEFAULT_MIN_LENGTH,
    max_length: DEFAULT_MAX_LENGTH,
    dump_data: false,
    output_format: DEFAULT_OUTPUT_FORMAT.to_string(),
    locale: String::new(),
    interactive: false,
    charset: String::from(DEFAULT_CHARSET),
    limit: DEFAULT_LIMIT,
    verbosity: DEFAULT_VERBOSITY,
    timeout: Some(std::time::Duration::from_secs_f32(GENERATOR_TIMEOUT_SECONDS)),
  };
  use crate::data::mock::MockSource;
  // unsafe {
  //   if !testing_logger_init {
  //     eprintln!("instanciating logger");
  //     // setup_logger("log4rs.testing.yml")?;
  //     testing_logger_init = true;
  //   }
  // }
  crate::data::setup_sources(&cfg)?;
  crate::data::sources_mut().push(Box::new(MockSource::open(SourceConfig::default())?));
  let mut sources: &'static mut Vec<BoxedSource> = crate::data::sources_mut();
  let mut source: &mut BoxedSource = sources.first_mut().unwrap();
  if let Some(func) = data_setup {
    func(source);
  }
  Ok(cfg.clone())
}
