use crate::config::DEFAULT_CHARSET;
use rand::Rng;

pub struct RandomReplace {
  trigger: String,
}


impl RandomReplace {
  pub fn new(t: String) -> RandomReplace {
    RandomReplace{
      trigger: t,
    }
  }

  pub fn replace<S: AsRef<str>, F: FnMut() -> String>(&self, s: S, mut f: F) -> String {
    let mut template: String = s.as_ref().to_string();
    let mut num: String;
    while let Some(pos) = template.find(self.trigger.as_str()) {
      num = f();
      template = template.replacen(self.trigger.as_str(), num.as_str(), 1);
    }
    template
  }

  pub fn replace_by_digits<S: AsRef<str>>(&mut self, s: S) -> String {
    let mut rng = rand::thread_rng();
    self.replace(s, || format!("{}", rng.gen_range(0, 9)))
  }

  pub fn replace_by_letters<S: AsRef<str>>(&mut self, s: S) -> String {
    let mut rng = rand::thread_rng();
    self.replace(s, || format!("{}", DEFAULT_CHARSET.chars().nth(rng.gen_range(0, DEFAULT_CHARSET.len())).unwrap()))
  }
}

