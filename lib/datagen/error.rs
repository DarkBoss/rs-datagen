#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Error {
  IO(String),
  SilentExit,
  InvalidOption{
    name: String,
    message: String,
  },
  UnknownGenerator(String),
  UnknownFormat(String),
  DataSourceError(String),
  Custom(String)
}

impl Error {
  pub fn title(&self) -> String {
    match self {
      Error::IO(_) => "IO",
      Error::Custom(_) => "custom",
      Error::SilentExit => "silent_exit",
      Error::InvalidOption{name, message} => "invalid option",
      Error::UnknownGenerator(_) => "unknown generator",
      Error::UnknownFormat(_) => "unknown format",
      Error::DataSourceError(_) => "data source",
    }.to_string()
  }

  pub fn message(&self) -> String {
    match self {
      Error::IO(msg) => msg.to_string(),
      Error::Custom(msg) => msg.to_string(),
      Error::SilentExit => "success".to_string(),
      Error::InvalidOption{name, message} => format!("invalid option '{}', {}", name, message),
      Error::UnknownGenerator(name) => name.to_string(),
      Error::UnknownFormat(name) => name.to_string(),
      Error::DataSourceError(msg) => msg.to_string(),
    }
  }
}

impl std::fmt::Display for Error {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{title}: {message}", title=self.title(), message=self.message())
  }
}

impl std::convert::From<std::io::Error> for Error {
  fn from(e: std::io::Error) -> Error {
    Error::IO(e.to_string())
  }
}