use locale_codes::language::*;
use crate::result::Result;
use crate::error::Error;

static mut locale: Option<&'static LanguageInfo> = None;

pub fn set_locale(lang: &str) -> Result<&'static LanguageInfo> {
  unsafe {
    locale = lookup(lang);
    locale.map_or(Err(Error::Custom(format!("unknown locale '{}'", lang))), |v| Ok(v))
  }
}

pub fn get_locale() -> &'static LanguageInfo {
  unsafe {
    if locale.is_none() {
      panic!("locale not defined");
    }
    locale.unwrap()
  }
}