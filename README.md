# Data Generator

Written in rust by Morgan Welsch @2020

This tool generates realistic datasets to use for testing, mocking...

The main principle is:
- Load datasets from DB, using the wanted locale
- Use generators to produce N items
- Write formatted items to output

To generate realistic data, some generators need an included **SQLite DB**, such as the
`person name` generator.

## Usage

```
USAGE:
    datagen [FLAGS] [OPTIONS]

FLAGS:
        --dump-data          Print data to stdout and exit
    -h, --help               Prints help information
    -i, --interactive        Interactively ask configuration values
        --list-formats       List available output formats
        --list-generators    List available generators
    -V, --version            Prints version information

OPTIONS:
    -f, --format <EXT>          Define output format [default: csv]
    -g, --generator <NAME>      Set the generator to use
    -L, --limit <LINE_COUNT>    Limit the number of generated lines
        --locale <eng>          Override system locale
    -M, --max <MAX_LENGTH>      Set the maximum length of the words to generate
    -m, --min <MIN_LENGTH>      Set the minimum length of the words to generate
        --verbose <verbose>     Show additionnal log messages [default: low]  [possible values: none, low, medium, high]
```

## Examples

### Generate json persons

```shell
./datagen -f json -g 'person'
```

### Generate csv phone numbers

```shell
./datagen -f csv -g 'phone number'
```

### Generate 

## Supported generators

| generator       | description                   |
|-----------------|-------------------------------|
| random password | randomly generated password   |
| person name     | localized name of person      |
| country name    | iso country names             |
| country code    | iso country codes             |
| phone number    | localized phone numbers       |
| health ssn      | social security number        |
| person          | person infos                  |
| person age      | age between 1 and 100         |
| health bpm      | cardiac rythm                 |

## Supported formats

| format                      | ext   |
|-----------------------------|-------|
| Comma Separated Values      | csv   |
| Javascript Object Notation  | json  |

## DB

The database schema can be found [here](data/db.sql).

| table      | description                                          |
|------------|------------------------------------------------------|
| dataset    | represents a group of data                           |
| locale     | represents a specific language                       |
| domain     | represents a logical domain (e.g: person, finance...)|
| data       | the data values used to generate complexe data       |
| migrations | represents the list of applied migrations            |

## Migrations

The `db.sh` script allows to quickly apply or revert all migrations using:
```bash
./db.sh --migrate --up --all
```
Or:
```bash
./db.sh --migrate --down --all
```

## How to build ?

- `cargo build --release` to generate the binary @`target/release/datagen`
- `cargo run` to build & run it in one go
